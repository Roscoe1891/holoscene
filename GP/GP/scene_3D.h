#ifndef _SCENE3D_H
#define _SCENE3D_H

#include <windows.h>
#include <stdio.h>
#include <mmsystem.h>
#include <math.h>
#include <gl\gl.h>
#include <gl\glu.h>

#include "utilities\SOIL.h"
#include "utilities\Input.h"
#include "model\Model.h"

#include "geometry\shape.h"
#include "geometry\shape_data.h"
#include "geometry\shape_factory.h"

#include "camera\camera.h"
#include "camera\camera_controller.h"
#include "camera\camera_controller_user.h"
#include "camera\camera_controller_security.h"

#include "lighting\light.h"
#include "Lighting\light_manager.h"
#include "lighting\ambient_light.h"
#include "lighting\directional_light.h"
#include "lighting\point_light.h"
#include "lighting\spot_light.h"
#include "lighting\material.h"

#include "scene\dance_floor.h"
#include "scene\wall_decoration.h"
#include "scene\mirrored_shelves.h"
#include "scene\fridge.h"
#include "scene\table.h"

// Scene 3D
// The main class for the 3D scene to be displayed in the window

namespace GP3D
{

#define COLOUR_DEPTH 16	//Colour depth
#define MAX_CAMERAS 3
#define MAX_WALLS 10
#define MAX_FLOORS 2
#define MAX_TABLES 3
#define MAX_LIGHTS 3

	struct Point
	{
		int x, y;
	};

	class Scene3D
	{
	public:
		enum CameraMode { MAIN, SECURITY, FIXED };

		void Init(HWND* hwnd, Input* in);	// initialse
		void Update(float dt);				// update scene
		void Render();						// render scene
		void Resize();						// resize scene window

	protected:
		bool CreatePixelFormat(HDC hdc);				// Create Pixel Format
		void ResizeGLWindow(int width, int height);		// Resize GL Window to width x height
		void InitializeOpenGL(int width, int height);	// Initialise OpenGL setting based of window of size width x height
		POINT CalcScreenCentre(RECT& screen);			// Calculate and return centre (screen co-ords)
		POINT CalcClientCentre(RECT& screen);			// Calculate and return centre (client co-ords)

		//vars
		HWND* hwnd_;									// handle to the window
		Input* input_;									// pointer to input
		RECT screen_rect_;								// rectangle formed by the window which forms the 'screen'
		POINT screen_centre_;							// the centre of the window in screen co-ords
		POINT client_centre_;							// the centre of the window in client co-ords
		HDC	hdc_;										// handle to device context
		HGLRC hrc_;										// hardware RENDERING CONTEXT
		
		ShapeFactory shape_factory_;					// The factory of shapes
		
		// Geometry
		Shape walls_[MAX_WALLS];
		Shape floors_[MAX_FLOORS];
		Shape ceiling_;

		// Scene Pieces
		DanceFloor dance_floor_;
		WallDecoration wall_decoration_[6];
		MirroredShelves mirrored_shelves_;
		Fridge fridge_;
		Table tables_[MAX_TABLES];

		// Models
		Model bar_;
		Model bottle_;
		Model cabinet_;
		Model can_;
		Model stool_;

		//Textures
		GLuint light_bar_texture_;
		GLuint wallpaper_texture1_;
		GLuint wallpaper_texture2_;
		GLuint carpet_texture_;
		GLuint wood_texture_;
		GLuint shadow_texture_;

		// Camera
		CameraMode camera_mode_;
		Camera cameras_[MAX_CAMERAS];										// The cameras 
		
		CameraControllerUser camera_controller_main_;					// The controller for the main camera
		CameraControllerSecurity camera_controller_security_panning_;	// The controller for panning security camera
		CameraControllerSecurity camera_controller_security_;			// The controller for security camera 2
		CameraController* camera_controllers_[MAX_CAMERAS];				// The camera controller

		LightManager light_manager_;

		bool wireframe_enabled_;

	};

}

#endif