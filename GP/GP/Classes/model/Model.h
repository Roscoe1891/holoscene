#ifndef _MODEL_H_
#define _MODEL_H_


#include <Windows.h>
#include <fstream>
#include <gl/gl.h>
#include <gl/glu.h>

#include "Utilities\token_stream.h"
#include <vector>
#include "maths\vector3.h"
#include "Utilities\SOIL.h"
#include "geometry\transform.h"

using namespace std;

namespace GP3D
{
	class Model
	{

	public:

		bool Load(char*, char*);
		void Render(bool draw_texture);

		inline Transform* transform() { return &transform_; }

	private:
		void LoadTexture(char*);
		bool LoadModel(char*);

		int vertex_count_;										// number of vertices for this model
		GLuint texture_;										// model texture

		vector<float> vertices_, normals_, texture_coords_;

		Transform transform_;									// model transform
	};
}

#endif