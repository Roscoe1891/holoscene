#ifndef _SHAPE_DATA_H
#define _SHAPE_DATA_H

#include <vector>
#include <windows.h>
#include "gl\gl.h"
#include "gl\glu.h"
#include "Maths\Vector3.h"

// Shape Data is used by Shape Factory as a container in the process of creating shapes

namespace GP3D
{
	using std::vector;

	class ShapeData
	{
	public:
		enum ShapeType { PLANE, DISC, CUBE, SPHERE, CYLINDER };

		ShapeData();
		~ShapeData();

		//inline vector<GLubyte>& indices() { return indices_; }
		inline vector<float>& vertices() { return vertices_; }
		inline vector<float>& normals() { return normals_; }
		inline vector<float>& texture_coords() { return texture_coords_; }

		inline int polygons() const { return polygons_; }
		inline void set_polygons(const int polygons) { polygons_ = polygons; }

		inline ShapeType shape_type() const { return shape_type_; }
		inline void set_shape_type(const ShapeType shape_type) { shape_type_ = shape_type; }
		
		inline Vector3 origin() const { return origin_; }
		inline void set_origin(const Vector3 origin) { origin_ = origin; }

	private:
		//vector<GLubyte> indices_;
		vector<float> vertices_;
		vector<float> normals_;
		vector<float> texture_coords_;

		ShapeType shape_type_;
		Vector3 origin_;				// Shape's initial origin
		int polygons_;
	};


}

#endif
