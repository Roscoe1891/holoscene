#include "transform.h"

namespace GP3D
{
	Transform::Transform()
	{
		translation_ = Vector3(0.f, 0.f, 0.f);
		rotation_ = Vector3(0.f, 0.f, 0.f);
		scale_ = Vector3(1.f, 1.f, 1.f);
	}

	Transform::~Transform()
	{
	}

	// Apply gl transformation
	void Transform::ApplyTransform()
	{
		glTranslatef(translation_.x(), translation_.y(), translation_.z());

		glRotatef(rotation_.x(), 1.0f, 0.0f, 0.0f);
		glRotatef(rotation_.y(), 0.0f, 1.0f, 0.0f);
		glRotatef(rotation_.z(), 0.0f, 0.0f, 1.0f);

		glScalef(scale_.x(), scale_.y(), scale_.z());
	}
}