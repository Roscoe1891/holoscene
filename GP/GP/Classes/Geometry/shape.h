#ifndef _SHAPE_H
#define _SHAPE_H

#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <vector>
#include "maths\math_definitions.h"
#include "maths\vector3.h"
#include "maths\vector4.h"
#include "maths\matrix44.h"
#include "Lighting\material.h"
#include "geometry\transform.h"
#include "shape_data.h"

using std::vector;

// A basic shape used for world geometry

namespace GP3D
{
	class Shape
	{
	public:
		enum RenderMethod { ARRAY_ELEMENT, DRAW_ARRAYS };

		Shape();
		~Shape();
		void Init(ShapeData* shape_data, Material* material = NULL, GLuint txr = NULL);
		void Render(RenderMethod renderMethod = DRAW_ARRAYS);
		void TileTextureCoords();

		// return shape's transform
		inline Transform* transform() { return &transform_; }

	private:
		//void CopyGLubyteData(vector<GLubyte>& data, vector<GLubyte>& dest);
		void CopyFloatData(vector<float>& data, vector<float>& dest);
		void CopyMaterial(Material* mat);

		//vector<GLubyte> indices_;
		vector<float> vertices_;
		vector<float> normals_;
		vector<float> texture_coords_;
	
		Material material_;
		GLuint texture_;
		Transform transform_;
	};
}

#endif