#include "shape_factory.h"

namespace GP3D
{
	ShapeFactory::ShapeFactory()
	{
	}

	ShapeFactory::~ShapeFactory()
	{
	}

	void ShapeFactory::CreateShape(Shape* shape, ShapeData::ShapeType type, int polys, Vector3 origin, Material* material, GLuint txr)
	{
		ShapeData* shape_dat = CreateShapeData(type, polys, origin);
		shape->Init(shape_dat, material, txr);
		delete shape_dat;
		shape_dat = NULL;
	}

	ShapeData* ShapeFactory::CreateShapeData(ShapeData::ShapeType type, int polys, Vector3 origin)
	{
		ShapeData* shape = new ShapeData();

		shape->set_shape_type(type);
		shape->set_polygons(polys);
		shape->set_origin(origin);

		switch (shape->shape_type())
		{
		case ShapeData::PLANE:
			CreatePlane(shape);
			break;
		case ShapeData::DISC:
			CreateDisc(shape);
			break;
		case ShapeData::CUBE:
			CreateCube(shape);
			break;
		case ShapeData::SPHERE:
			CreateSphere(shape);
			break;
		case ShapeData::CYLINDER:
			CreateCylinder(shape);
			break;
		}

		return shape;
	}

	void ShapeFactory::CreatePlane(ShapeData* shape, char normal, bool face_up, bool cube_component)		// params for use by createcube function only (see definition)
	{
		for (int y = 0; y < shape->polygons(); ++y)
		{
			for (int x = 0; x < shape->polygons(); ++x)
			{
				// NORMALS

				// Set normal as vector3
				Vector3 norm;
				switch (normal)
				{
				case 'x':
					norm = Vector3(1.0, 0.0, 0.0);
					//if (face_up) norm = Vector3(1.0, 0.0, 0.0);
					//else norm = Vector3(-1.0, 0.0, 0.0);
					break;
				case 'y':
					norm = Vector3(0.0, 1.0, 0.0);
					//if (face_up) norm = Vector3(0.0, 1.0, 0.0);
					//else norm = Vector3(0.0, -1.0, 0.0);
					break;
				case 'z':
					norm = Vector3(0.0, 0.0, 1.0);
					//if (face_up) norm = Vector3(0.0, 0.0, 1.0);
					//else norm = Vector3(0.0, 0.0, -1.0);
					break;
				default:
					break;
				}

				// add components to normals vector 
				for (int i = 0; i < 6; ++i)
				{
					shape->normals().push_back(norm.x());
					shape->normals().push_back(norm.y());
					shape->normals().push_back(norm.z());
				}

				// VERTICES
				Matrix44 verts;						// 4x4 Matrix to store the 4 vertices

				float offset = shape->polygons() * 0.5f;		// offset ensures origin of quad is true origin

				// populate matrix based on a 'default' quad
				// (drawn in the xy plane, facing camera)
				verts.m[0][0] = x + 1.0f - offset;
				verts.m[0][1] = y + 1.0f - offset;
				verts.m[0][2] = 1.0f - offset;
				verts.m[0][3] = 1.0f;

				verts.m[1][0] = x - offset;
				verts.m[1][1] = y + 1.0f - offset;
				verts.m[1][2] = 1.0f - offset;
				verts.m[1][3] = 1.0f;

				verts.m[2][0] = x - offset;
				verts.m[2][1] = y - offset;
				verts.m[2][2] = 1.0f - offset;
				verts.m[2][3] = 1.0f;

				verts.m[3][0] = x + 1.0f - offset;
				verts.m[3][1] = y - offset;
				verts.m[3][2] = 1.0f - offset;
				verts.m[3][3] = 1.0f;

				// perform rotations if necessary
				// (i.e. if quad is not 'default' quad)
				Matrix44 rotation_matrix;
				rotation_matrix.SetIdentity();

				Vector3 translation = shape->origin();
				Vector3 translation_offset = norm;
				translation_offset.Scale(shape->polygons() - 1.0f);

				switch (normal)
				{
				case 'x':
					if (face_up)
					{
						rotation_matrix.RotationY((float)PI * 0.5f);
						translation += translation_offset;
					}
					else
					{
						rotation_matrix.RotationY(-(float)PI * 0.5f);
						translation -= translation_offset;
					}
					break;
				case 'y':
					if (face_up)
					{
						rotation_matrix.RotationX(-(float)PI * 0.5f);
						translation += translation_offset;
					}
					else
					{
						rotation_matrix.RotationX((float)PI * 0.5f);
						translation -= translation_offset;
					}
					break;
				case 'z':
					if (face_up)
					{
						if (cube_component) translation += translation_offset;
					}
					else
					{
						rotation_matrix.RotationY((float)PI);
						translation -= translation_offset;
					}
					break;
				}

				verts = verts * rotation_matrix;

				// translate vertices to init origin
				Matrix44 translate_matrix;
				translate_matrix.SetIdentity();
				translate_matrix.SetTranslation(translation);
				verts = verts * translate_matrix;

				// add vertices to vertex vector
				for (int v = 0; v < 3; ++v) shape->vertices().push_back(verts.m[0][v]);

				for (int v = 0; v < 3; ++v) shape->vertices().push_back(verts.m[1][v]);

				for (int v = 0; v < 3; ++v) shape->vertices().push_back(verts.m[3][v]);

				for (int v = 0; v < 3; ++v) shape->vertices().push_back(verts.m[2][v]);

				for (int v = 0; v < 3; ++v) shape->vertices().push_back(verts.m[3][v]);

				for (int v = 0; v < 3; ++v) shape->vertices().push_back(verts.m[1][v]);


				// TEXTURE COORDS
				float uv_increment = 1.0f / shape->polygons();
				float u_pos = uv_increment * (float)x;
				float v_pos = uv_increment * (shape->polygons() - ((float)y + 1.0f));

				shape->texture_coords().push_back(u_pos + uv_increment);
				shape->texture_coords().push_back(v_pos);

				shape->texture_coords().push_back(u_pos);
				shape->texture_coords().push_back(v_pos);

				shape->texture_coords().push_back(u_pos + uv_increment);
				shape->texture_coords().push_back(v_pos + uv_increment);

				shape->texture_coords().push_back(u_pos);
				shape->texture_coords().push_back(v_pos + uv_increment);

				shape->texture_coords().push_back(u_pos + uv_increment);
				shape->texture_coords().push_back(v_pos + uv_increment);

				shape->texture_coords().push_back(u_pos);
				shape->texture_coords().push_back(v_pos);
			}
		}
	}
	
	// Create a Cube comprised of 6 planes
	void ShapeFactory::CreateCube(ShapeData* shape)
	{
		CreatePlane(shape, 'x', false, true);
		CreatePlane(shape, 'x', true, true);

		CreatePlane(shape, 'y', false, true);
		CreatePlane(shape, 'y', true, true);

		CreatePlane(shape, 'z', false, true);
		CreatePlane(shape, 'z', true, true);
	}
	
	// Create a disc
	void ShapeFactory::CreateDisc(ShapeData* shape)
	{
		float angle_offset = (2 * (float)PI) / shape->polygons();

		// define normal (positive along z axis)
		Vector3 norm;
		norm = Vector3(0.0f, 0.0f, 1.0f);

		double angle;
		float x, y, u, v;

		// calculate vertices and tex co-ords
		for (int i = 0; i < shape->polygons(); ++i)
		{
			for (int iv = 0; iv < 3; ++iv) { shape->vertices().push_back(0.0f); }
			for (int it = 0; it < 2; ++it) { shape->texture_coords().push_back(0.5f); }
				
			angle = (angle_offset * i);
			x = (float)cos(angle);
			y = (float)sin(angle);
			u = (-x / 2) + 0.5f;
			v = (y / 2) + 0.5f;

			shape->vertices().push_back(x);
			shape->vertices().push_back(y);
			shape->vertices().push_back(0.0f);

			shape->texture_coords().push_back(u);
			shape->texture_coords().push_back(v);

			if (i + 1 == shape->polygons()) angle = 0.0f;
			else angle = (angle_offset * (i + 1));
			x = (float)cos(angle);
			y = (float)sin(angle);
			u = (-x / 2) + 0.5f;
			v = (y / 2) + 0.5f;

			shape->vertices().push_back(x);
			shape->vertices().push_back(y);
			shape->vertices().push_back(0.0f);

			shape->texture_coords().push_back(u);
			shape->texture_coords().push_back(v);
		}

		// all vertices have the same normal
		for (size_t in = 0; in < shape->vertices().size(); in +=3)
		{
			shape->normals().push_back(norm.x());
			shape->normals().push_back(norm.y());
			shape->normals().push_back(norm.z());
		}

		// translate vertices to shape's origin
		for (size_t iv = 0; iv < shape->vertices().size(); iv += 3)
		{
			shape->vertices()[iv] += shape->origin().x();
			shape->vertices()[iv + 1] += shape->origin().y();
			shape->vertices()[iv + 2] += shape->origin().z();
		}
			
	}

	// Create a sphere
	void ShapeFactory::CreateSphere(ShapeData* shape)
	{
		const float lats_interval = 1.f / shape->polygons();
		const float longs_interval = 1.f / shape->polygons();

		const float theta = GetTheta(shape->polygons());
		const float delta = GetDelta(shape->polygons());

		for (int longs = 0; longs < shape->polygons(); ++longs)
		{
			for (int lats = 0; lats < shape->polygons(); ++lats)
			{
				// VERTICES
				shape->vertices().push_back(GetX(lats, longs, theta, delta));
				shape->vertices().push_back(GetY(longs, delta));
				shape->vertices().push_back(GetZ(lats, longs, theta, delta));

				shape->vertices().push_back(GetX(lats + 1, longs + 1, theta, delta));
				shape->vertices().push_back(GetY(longs + 1, delta));
				shape->vertices().push_back(GetZ(lats + 1, longs + 1, theta, delta));

				shape->vertices().push_back(GetX(lats, longs + 1, theta, delta));
				shape->vertices().push_back(GetY(longs + 1, delta));
				shape->vertices().push_back(GetZ(lats, longs + 1, theta, delta));

				shape->vertices().push_back(GetX(lats + 1, longs + 1, theta, delta));
				shape->vertices().push_back(GetY(longs + 1, delta));
				shape->vertices().push_back(GetZ(lats + 1, longs + 1, theta, delta));

				shape->vertices().push_back(GetX(lats, longs, theta, delta));
				shape->vertices().push_back(GetY(longs, delta));
				shape->vertices().push_back(GetZ(lats, longs, theta, delta));

				shape->vertices().push_back(GetX(lats + 1, longs, theta, delta));
				shape->vertices().push_back(GetY(longs, delta));
				shape->vertices().push_back(GetZ(lats + 1, longs, theta, delta));

				// TEXTURE COORDS
				shape->texture_coords().push_back(GetU(lats_interval, lats));
				shape->texture_coords().push_back(GetV(longs_interval, longs));

				shape->texture_coords().push_back(GetU(lats_interval, lats + 1));
				shape->texture_coords().push_back(GetV(longs_interval, longs + 1));

				shape->texture_coords().push_back(GetU(lats_interval, lats));
				shape->texture_coords().push_back(GetV(longs_interval, longs + 1));

				shape->texture_coords().push_back(GetU(lats_interval, lats + 1));
				shape->texture_coords().push_back(GetV(longs_interval, longs + 1));

				shape->texture_coords().push_back(GetU(lats_interval, lats));
				shape->texture_coords().push_back(GetV(longs_interval, longs));
					
				shape->texture_coords().push_back(GetU(lats_interval, lats + 1));
				shape->texture_coords().push_back(GetV(longs_interval, longs));
			}
		}

		// NORMALS
		// (normals are the same as vertices here)
		for (size_t n = 0; n < shape->vertices().size(); ++n)
		{
			shape->normals().push_back(shape->vertices()[n]);
		}

		// Move Sphere to init position
		for (size_t v = 0; v < shape->vertices().size(); v += 3)
		{
			shape->vertices()[v] += shape->origin().x();
			shape->vertices()[v + 1] += shape->origin().y();
			shape->vertices()[v + 2] += shape->origin().z();
		}
	}
	
	// Create a cylinder
	void ShapeFactory::CreateCylinder(ShapeData* shape)
	{
		// VERTICES
		const float longSegments = 4;
		const float angle_interval = (float)((2 * PI) / shape->polygons());

		const float lats_interval = 1.f / shape->polygons();
		const float longs_interval = 1.f / longSegments;

		for (int longs = 0; longs < longSegments; ++longs)
		{
			for (int lats = 0; lats < shape->polygons(); ++lats)
			{
				double angle;
				float x, z;

				angle = angle_interval * (lats + 1);	// 0
				x = float(-cos(angle));
				z = float(sin(angle));
				shape->vertices().push_back(x);
				shape->vertices().push_back((float)(longs + 1));
				shape->vertices().push_back(z);

				angle = angle_interval * lats;			// 1
				x = float(-cos(angle));
				z = float(sin(angle));
				shape->vertices().push_back(x);
				shape->vertices().push_back((float)(longs + 1));
				shape->vertices().push_back(z);

				angle = angle_interval * lats;			// 3
				x = float(-cos(angle));
				z = float(sin(angle));
				shape->vertices().push_back(x);
				shape->vertices().push_back((float)longs);
				shape->vertices().push_back(z);

				angle = angle_interval * (lats + 1);	// 2
				x = float(-cos(angle));
				z = float(sin(angle));
				shape->vertices().push_back(x);
				shape->vertices().push_back((float)longs);
				shape->vertices().push_back(z);

				angle = angle_interval * (lats + 1);	// 3
				x = float(-cos(angle));
				z = float(sin(angle));
				shape->vertices().push_back(x);
				shape->vertices().push_back((float)(longs + 1));
				shape->vertices().push_back(z);

				angle = angle_interval * lats;			// 0
				x = float(-cos(angle));
				z = float(sin(angle));
				shape->vertices().push_back(x);
				shape->vertices().push_back((float)longs);
				shape->vertices().push_back(z);

				// TEXTURE COORDS

				shape->texture_coords().push_back(GetU(lats_interval, lats + 1));
				shape->texture_coords().push_back(GetV(longs_interval, longs + 1));

				shape->texture_coords().push_back(GetU(lats_interval, lats));
				shape->texture_coords().push_back(GetV(longs_interval, longs + 1));

				shape->texture_coords().push_back(GetU(lats_interval, lats));
				shape->texture_coords().push_back(GetV(longs_interval, longs));

				shape->texture_coords().push_back(GetU(lats_interval, lats + 1));
				shape->texture_coords().push_back(GetV(longs_interval, longs));

				shape->texture_coords().push_back(GetU(lats_interval, lats + 1));
				shape->texture_coords().push_back(GetV(longs_interval, longs + 1));

				shape->texture_coords().push_back(GetU(lats_interval, lats));
				shape->texture_coords().push_back(GetV(longs_interval, longs));
			}
		}

		// NORMALS
		// (normals are the same as vertices here)
		for (size_t in = 0; in < shape->vertices().size(); ++in)
		{
			shape->normals().push_back(shape->vertices()[in]);
		}

		// Move Cylinder to init position
		for (size_t iv = 0; iv < shape->vertices().size(); iv += 3)
		{
			shape->vertices()[iv] += (shape->origin().x());
			shape->vertices()[iv + 1] += shape->origin().y() - (longSegments * 0.5f);
			shape->vertices()[iv + 2] += shape->origin().z();
		}
	}

	// UTILITY FUNCTIONS FOR DISC / SPHERE

	float ShapeFactory::GetTheta(int latitude)
	{
		float theta = 2 * (float)PI / latitude;
		return theta;
	}

	float ShapeFactory::GetDelta(int longitude)
	{
		float delta = (float)PI / longitude;
		return delta;
	}

	float ShapeFactory::GetX(int latitude, int longitude, float theta, float delta)
	{
		float x = cos(theta * latitude) * sin(delta * longitude);
		return x;
	}

	float ShapeFactory::GetY(int longitude, float delta)
	{
		float y = cos(delta * longitude);
		return y;
	}

	float ShapeFactory::GetZ(int latitude, int longitude, float theta, float delta)
	{
		float z = sin(theta * latitude) * sin(delta * longitude);
		return z;
	}

	float ShapeFactory::GetU(float lat_spacing, int latitude)
	{
		float u = lat_spacing * latitude;
		return u;
	}

	float ShapeFactory::GetV(float long_spacing, int longitude)
	{
		float v = long_spacing * longitude;
		return v;
	}



}