#include "Shape.h"

namespace GP3D
{
	Shape::Shape()
	{
		texture_ = NULL;
	}

	Shape::~Shape()
	{
	}

	void Shape::Init(ShapeData* shape_data, Material* material, GLuint txr)
	{
		CopyFloatData(shape_data->vertices(), vertices_);
		CopyFloatData(shape_data->normals(), normals_);
		CopyFloatData(shape_data->texture_coords(), texture_coords_);

		if (material != NULL) CopyMaterial(material);
		if (txr != NULL) texture_ = txr;
	}

	void Shape::CopyFloatData(vector<float>& data, vector<float>& dest)
	{
		for (size_t i = 0; i < data.size(); i++)
		{
			dest.push_back(data[i]);
		}
	}

	void Shape::CopyMaterial(Material* mat)
	{
		material_.Init(mat->ambient(), mat->diffuse(), mat->specular(), mat->emission(), mat->shininess());
	}

	void Shape::TileTextureCoords()
	{
		for (size_t i = 0; i < texture_coords_.size(); i++)
		{
			texture_coords_[i] = 1.f;
			texture_coords_[i += 1] = 0.f;
			//shape->texture_coords().push_back(u_pos + uv_increment);
			//shape->texture_coords().push_back(v_pos);

			texture_coords_[i += 1] = 0.f;
			texture_coords_[i += 1] = 0.f;
			//shape->texture_coords().push_back(u_pos);
			//shape->texture_coords().push_back(v_pos);

			texture_coords_[i += 1] = 1.f;
			texture_coords_[i += 1] = 1.f;
			//shape->texture_coords().push_back(u_pos + uv_increment);
			//shape->texture_coords().push_back(v_pos + uv_increment);

			texture_coords_[i += 1] = 0.f;
			texture_coords_[i += 1] = 1.f;
			//shape->texture_coords().push_back(u_pos);
			//shape->texture_coords().push_back(v_pos + uv_increment);

			texture_coords_[i += 1] = 1.f;
			texture_coords_[i += 1] = 1.f;
			//shape->texture_coords().push_back(u_pos + uv_increment);
			//shape->texture_coords().push_back(v_pos + uv_increment);

			texture_coords_[i += 1] = 0.f;
			texture_coords_[i += 1] = 0.f;
			//shape->texture_coords().push_back(u_pos);
			//shape->texture_coords().push_back(v_pos);
		}
	}

	void Shape::Render(RenderMethod renderMethod)
	{
		// Enable arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		//if (renderMethod == DRAW_ELEMENT) glEnableClientState(GL_INDEX_ARRAY);

		// Set array pointers
		glVertexPointer(3, GL_FLOAT, 0, vertices_.data());
		glNormalPointer(GL_FLOAT, 0, normals_.data());
		glTexCoordPointer(2, GL_FLOAT, 0, texture_coords_.data());
		//if (renderMethod == DRAW_ELEMENT) glIndexPointer(GL_UNSIGNED_BYTE, 0, indices_.data());

		glPushMatrix();
		{
			// Set texture
			if (texture_ != NULL)
			{
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, texture_);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			}
			else
			{
				glDisable(GL_TEXTURE_2D);
			}

			// Apply material
			material_.ApplyMaterial();

			// Apply transform
			transform_.ApplyTransform();

			//int vertexCount = vertex_multiplier * polygons * polygons;
			int vertexCount = vertices_.size() / 3;

			switch (renderMethod)
			{
			case ARRAY_ELEMENT:
				glBegin(GL_TRIANGLES);

				for (int i = 0; i < vertexCount; i++)
				{
					glArrayElement(i);
				}

				glEnd();
				break;

			case DRAW_ARRAYS:
				glDrawArrays(GL_TRIANGLES, 0, vertexCount);
				break;

				/*case DRAW_ELEMENT:
				glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_BYTE, indices_.data());
				break;*/
			}
		}
		glPopMatrix();

		// disable arrays
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		//if (renderMethod == DRAW_ELEMENT) glDisableClientState(GL_INDEX_ARRAY);
	}
}

