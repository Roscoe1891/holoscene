#ifndef _TRANSFORM_H
#define _TRANSFORM_H

// A transform can be applied to geometry to alter its position within the scene via gl translate, rotate and scale 

#include "Maths\Vector3.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h> 

// Transform is used to apply translation, rotation and scale to an object using the gl transformation functions

namespace GP3D
{
	class Transform
	{
	public:
		Transform();
		~Transform();
		void ApplyTransform();

		// param in new translation
		inline void set_translation(Vector3 translation) { translation_ = translation; }
		// param in new translation x component
		inline void set_translation_x(float x) { translation_.set_x(x); }
		// param in new translation y component
		inline void set_translation_y(float y) { translation_.set_y(y); }
		// param in new translation z component
		inline void set_translation_z(float z) { translation_.set_z(z); }
		// return translation
		inline Vector3 translation() const { return translation_; }

		// param in new rotation
		inline void set_rotation(Vector3 rotation) { rotation_ = rotation; }
		// param in new rotation about x axis
		inline void set_rotation_x(float x) { rotation_.set_x(x); }
		// param in new rotation about y axis
		inline void set_rotation_y(float y) { rotation_.set_y(y); }
		// param in new rotation about z axis
		inline void set_rotation_z(float z) { rotation_.set_z(z); }

		// param in new scale
		inline void set_scale(Vector3 scale) { scale_ = scale; }
		// param in new scale along x axis
		inline void set_scale_x(float x) { scale_.set_x(x); }
		// param in new scale along y axis
		inline void set_scale_y(float y) { scale_.set_y(y); }
		// param in new scale along z axis
		inline void set_scale_z(float z) { scale_.set_z(z); }

	private:
		Vector3 translation_, rotation_, scale_;
	};
}

#endif