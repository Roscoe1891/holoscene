#ifndef _SHAPE_FACTORY_H
#define _SHAPE_FACTORY_H

#include "shape.h"
#include "shape_data.h"
#include "maths\math_definitions.h"
#include "maths\vector3.h"
#include "maths\vector4.h"
#include "maths\matrix44.h"

// Shape Factory creates procedurally generated shapes 

namespace GP3D
{
	class ShapeFactory
	{
	public:
		ShapeFactory();
		~ShapeFactory();

		void CreateShape(Shape* shape, ShapeData::ShapeType type, int polys, Vector3 origin = Vector3(0.0, 0.0, 0.0), Material* material = NULL, GLuint txr = NULL);

	private:
		// Shape Creation Functions
		ShapeData* CreateShapeData(ShapeData::ShapeType type, int polys, Vector3 origin = Vector3(0.0, 0.0, 0.0));

		void CreatePlane(ShapeData* shape, char normal = 'z', bool face_up = true, bool cube_component = false);		// params for use by generatecube function only (see definition)
		void CreateDisc(ShapeData* shape);
		void CreateCube(ShapeData* shape);
		void CreateSphere(ShapeData* shape);
		void CreateCylinder(ShapeData* shape);

		// Sphere Utility Functions
		float GetTheta(int latitude);
		float GetDelta(int longitude);
		float GetX(int latitude, int longitude, float theta, float delta);
		float GetY(int longitude, float delta);
		float GetZ(int latitude, int longitude, float theta, float delta);
		float GetU(float lat_spacing, int latitude);
		float GetV(float long_spacing, int longitude);
	};
}

#endif