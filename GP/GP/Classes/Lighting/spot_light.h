#ifndef _SPOT_LIGHT_H
#define _SPOT_LIGHT_H

#include "Light.h"
#include "Maths/Vector3.h"

namespace GP3D
{
	class SpotLight : public Light
	{
	public:
		SpotLight();
		~SpotLight();
		virtual void Update(float dt);

		void Init(GLenum light, LightData& amb_data, LightData& diff_data, LightData& spec_data, Vector3 pos, Vector3 dir, Vector3 att, GLfloat cutoff, GLfloat expo);
		void SetPosition(LightData& pos_data);
		void SetPosition(Vector3 pos);
		void SetDirection(Vector3 dir);
		void Render();

		inline GLfloat(&direction())[3] { return direction_; }
		inline LightData& position() { return position_; }
		inline LightData& specular() { return specular_; }
		inline Vector3& attenuation() { return attenuation_; }

		inline const GLfloat& cut_off() const { return cut_off_; }
		inline const GLfloat& exponent() const { return exponent_; }

	private:
		LightData position_;
		LightData specular_;
		GLfloat direction_[3];
		Vector3 attenuation_;
		GLfloat cut_off_;
		GLfloat exponent_;

	};

	
}

#endif // !_SPOT_LIGHT_H
