#ifndef _LIGHT_H
#define _LIGHT_H

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

// The base class from which all lights are drived

namespace GP3D
{
	typedef GLfloat LightData[4];		

	class Light
	{
	public:
		enum LightType { AMBIENT, DIRECTIONAL, POINT, SPOT };
		Light();
		virtual~Light();

		virtual void Update(float dt) = 0;

		void Init(GLenum light);
		void SetAmbient(LightData& amb);
		void SetDiffuse(LightData& diff);

		// return light type
		inline LightType light_type() const { return light_type_; }
		// return ambient light data
		inline LightData& ambient() { return ambient_; }
		// return diffuse light data
		inline LightData& diffuse() { return diffuse_; }
		// retrun GL light
		inline GLenum GL_light() const { return GL_light_; }

		
	protected:
		void CopyLightData(LightData data, LightData& destination);

		const int k_light_data_max_;	// maximum number of components in a light data array 
		LightType light_type_;			// this light's type
		LightData ambient_, diffuse_;

		GLenum GL_light_;				// the OpenGL light assigned to this light
	};
}

#endif