#ifndef _MATERIAL_H
#define _MATERIAL_H

#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include "light.h"

// Material defines how an object reflects light 

namespace GP3D
{
	using std::string;
	using std::streampos;
	using std::streamsize;
	using std::ifstream;

	class Material
	{
	public:
		enum MaterialType { MATT, METAL, GLOW };		// material "preset" types

		Material();
		~Material();
		void Init(LightData& amb, LightData& diff, LightData& spec, LightData& emis, GLfloat shiny);
		void Init(MaterialType type);
		void ApplyMaterial();

		// return ambient 
		inline LightData& ambient() { return ambient_; }
		// return diffuse
		inline LightData& diffuse() { return diffuse_; }
		// return specular
		inline LightData& specular() { return specular_; }
		// return emission
		inline LightData& emission() { return emission_; }
		// return shininess
		inline GLfloat shininess() const { return shininess_; }

	private:
		void CopyLightData(LightData data, LightData& destination);

		const int k_light_data_max_;		// max components in a light data object

		LightData ambient_;
		LightData diffuse_;
		LightData specular_;
		LightData emission_;
		GLfloat shininess_;

	};



}

#endif