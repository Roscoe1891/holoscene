#include "directional_light.h"

namespace GP3D
{
	DirectionalLight::DirectionalLight()
	{
	}

	DirectionalLight::~DirectionalLight()
	{
	}

	void DirectionalLight::Init(GLenum light, LightData& diff_data, LightData& spec_data, Vector3 pos)
	{
		Light::Init(light);

		light_type_ = DIRECTIONAL;

		CopyLightData(diff_data, diffuse_);
		CopyLightData(spec_data, specular_);

		ambient_[0] = 0.0f;
		ambient_[1] = 0.0f;
		ambient_[2] = 0.0f;
		ambient_[3] = 1.0f;

		pos.Normalise();
		SetPosition(pos);

		glLightfv(GL_light_, GL_AMBIENT, ambient_);
		glLightfv(GL_light_, GL_DIFFUSE, diffuse_);
		glLightfv(GL_light_, GL_SPECULAR, specular_);
		glLightfv(GL_light_, GL_POSITION, position_);

		glEnable(GL_light_);
	}

	void DirectionalLight::Update(float dt)
	{
		//glLightfv(GL_light_, GL_POSITION, position_);
	}

	void DirectionalLight::SetPosition(LightData& pos_data)
	{
		CopyLightData(pos_data, position_);
	}

	void DirectionalLight::SetPosition(Vector3 pos)
	{ 
		position_[0] = (GLfloat)pos.x();
		position_[1] = (GLfloat)pos.y();
		position_[2] = (GLfloat)pos.z();
		position_[3] = 0.0f;
	}


}
