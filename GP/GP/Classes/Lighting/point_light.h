#ifndef _POINT_LIGHT_H
#define _POINT_LIGHT_H

#include "light.h"
#include "maths\vector3.h"
#include "Geometry\shape_factory.h"

// A point light emits light from its position in all directions

namespace GP3D
{
	class PointLight : public Light
	{
	public:
		PointLight();
		~PointLight();
		virtual void Update(float dt);

		void Init(GLenum light, ShapeFactory* shape_factory, LightData& amb_data, LightData& diff_data, LightData& spec_data, Vector3 pos, Vector3 att);
		void SetPosition(LightData& pos_data);
		void SetPosition(Vector3 pos);
		void RenderLight();				// Renders a sphere at this light's position
		void Render();					// Refresh light values for rendering

		// return position
		inline LightData& position() { return position_; }
		// return specular
		inline LightData& specular() { return specular_; }
		// return attenuation
		inline Vector3& attenuation() { return attenuation_; }


	private:
		Shape sphere_;				// sphere used as a marker for the light
		LightData specular_;		// specular light data
		LightData position_;		// position light data
		Vector3 attenuation_;		// attenuation values
	};
}

#endif