#include "spot_light.h"

namespace GP3D
{
	SpotLight::SpotLight()
	{
		cut_off_ = 0.f;
		exponent_ = 0.f;
	}

	SpotLight::~SpotLight()
	{
	}

	void SpotLight::Init(GLenum light, LightData& amb_data, LightData& diff_data, LightData& spec_data, Vector3 pos, Vector3 dir, Vector3 att, GLfloat cutoff, GLfloat expo)
	{
		Light::Init(light);

		CopyLightData(amb_data, ambient_);
		CopyLightData(diff_data, diffuse_);
		CopyLightData(spec_data, specular_);

		SetPosition(pos);
		SetDirection(dir);
		
		attenuation_ = att;
		cut_off_ = cutoff;
		exponent_ = expo;

		// Initialise spotlight settings
		glEnable(GL_LIGHTING);
		
		glLightfv(GL_light_, GL_AMBIENT, ambient_);
		glLightfv(GL_light_, GL_DIFFUSE, diffuse_);
		glLightfv(GL_light_, GL_POSITION, position_);

		glLightf(GL_light_, GL_SPOT_CUTOFF, cut_off_);
		glLightfv(GL_light_, GL_SPOT_DIRECTION, direction_);
		glLightf(GL_light_, GL_SPOT_EXPONENT, exponent_);

		glLightf(GL_light_, GL_CONSTANT_ATTENUATION, attenuation_.x());
		glLightf(GL_light_, GL_LINEAR_ATTENUATION, attenuation_.y());
		glLightf(GL_light_, GL_QUADRATIC_ATTENUATION, attenuation_.z());

		glEnable(GL_light_);
	}


	// A spotlight must have position and direction updated each frame
	void SpotLight::Update(float dt)
	{
		glLightfv(GL_light_, GL_AMBIENT, ambient_);
		glLightfv(GL_light_, GL_DIFFUSE, diffuse_);
		glLightfv(GL_light_, GL_POSITION, position_);

		glLightf(GL_light_, GL_SPOT_CUTOFF, cut_off_);
		glLightfv(GL_light_, GL_SPOT_DIRECTION, direction_);
		glLightf(GL_light_, GL_SPOT_EXPONENT, exponent_);

		glLightf(GL_light_, GL_CONSTANT_ATTENUATION, attenuation_.x());
		glLightf(GL_light_, GL_LINEAR_ATTENUATION, attenuation_.y());
		glLightf(GL_light_, GL_QUADRATIC_ATTENUATION, attenuation_.z());
	}

	void SpotLight::SetPosition(LightData& pos_data)
	{
		CopyLightData(pos_data, position_);
	}

	void SpotLight::SetPosition(Vector3 pos)
	{
		position_[0] = (GLfloat)pos.x();
		position_[1] = (GLfloat)pos.y();
		position_[2] = (GLfloat)pos.z();
		position_[3] = 0.0f;
	}

	void SpotLight::SetDirection(Vector3 dir)
	{
		direction_[0] = (GLfloat)dir.x();
		direction_[1] = (GLfloat)dir.y();
		direction_[2] = (GLfloat)dir.z();
	}

	void SpotLight::Render()
	{
		glPushMatrix();
		glTranslatef(position_[0], position_[1], position_[2]);
		gluSphere(gluNewQuadric(), 1.0, 8, 8);
		glPopMatrix();
	}

}