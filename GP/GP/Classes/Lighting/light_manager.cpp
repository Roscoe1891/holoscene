#include "light_manager.h"

namespace GP3D
{
	LightManager::LightManager()
	{
		flashing_ = false;
		flash_interval_ = 0.5f;
		flash_timer_ = 0.f;
	}

	LightManager::~LightManager()
	{
	}

	void LightManager::Init(ShapeFactory* shape_factory)
	{
		// Initialise lights
		LightData amb0 = { 0.2f, 0.2f, 0.2f, 1.f };
		LightData diff0 = { 0.f, 0.5f, 0.5f, 1.0f };					// Blue Light
		LightData spec0 = { 1.0f, 1.0f, 1.0f, 1.0f };
		Vector3 pos0 = Vector3(6.0f, 5.f, -17.0f);
		Vector3 att0 = Vector3(2.f, 0.2f, 0.0f);
		lights_[0].Init(GL_LIGHT0, shape_factory, amb0, diff0, spec0, pos0, att0);

		LightData amb1 = { 0.2f, 0.2f, 0.2f, 1.f };
		LightData diff1 = { 0.6f, 0.f, 0.6f, 1.0f };					// Magenta Light
		LightData spec1 = { 1.0f, 1.0f, 1.0f, 1.0f };
		Vector3 pos1 = Vector3(-3.0f, 5.f, 17.0f);
		Vector3 att1 = Vector3(2.f, 0.2f, 0.0f);
		lights_[1].Init(GL_LIGHT1, shape_factory, amb1, diff1, spec1, pos1, att1);

		LightData amb2 = { 0.4f, 0.4f, 0.4f, 1.f };
		LightData diff2 = { 0.8f, 0.8f, 0.8f, 1.0f };					// White Light
		LightData spec2 = { 1.0f, 1.0f, 1.0f, 1.0f };
		Vector3 pos2 = Vector3(0.0f, 1.f, 0.0f);
		Vector3 att2 = Vector3(1.f, 0.2f, 0.0f);
		lights_[2].Init(GL_LIGHT2, shape_factory, amb2, diff2, spec2, pos2, att2);

		// Set initial lighting
		light_type_ = BLUE;
		glEnable(lights_[light_type_].GL_light());

		flash_interval_ = 0.5f;
		flash_timer_ = 0.f;
		flashing_ = false;

	}

	void LightManager::Update(float& dt)
	{
		if (flashing_)								// if lights are flashing
		{
			if (flash_timer_ < flash_interval_)
			{
				flash_timer_ += dt;					// increment timer
			}
			else
			{
				glDisable(lights_[light_type_].GL_light());
				
				if (light_type_ == RED)				// change light type
					light_type_ = BLUE;
				else
					light_type_ = RED;

				glEnable(lights_[light_type_].GL_light());
				
				flash_timer_ = 0.f;
			}
		}
	}

	// Switch Light Mode dependant on current light mode
	void LightManager::SwitchLightMode()
	{
		glDisable(lights_[light_type_].GL_light());

		switch (light_type_)
		{
		case GP3D::LightManager::BLUE:
			light_type_ = RED;
			break;
		case GP3D::LightManager::RED:
			light_type_ = FLAT;
			break;
		case GP3D::LightManager::FLAT:
			light_type_ = BLUE;
			break;
		default:
			break;
		}

		glEnable(lights_[light_type_].GL_light());

	}

	// Render spheres at light locations
	void LightManager::RenderLight()
	{
		for (int i = 0; i < MAX_LIGHTS; ++i)
		{
			lights_[i].RenderLight();
		}
	}

	// Apply light values during render
	void LightManager::Render()
	{
		for (int i = 0; i < MAX_LIGHTS; ++i)
		{
			lights_[i].Render();
		}
	}
}