#include "material.h"

namespace GP3D
{
	Material::Material() : k_light_data_max_(4)
	{
	}

	Material::~Material()
	{
	}

	// Initialise a material of type "type"
	void Material::Init(MaterialType type)
	{
		switch (type)
		{
		case GP3D::Material::MATT:
		{
			LightData mat_amb = { 0.1f, 0.1f, 0.1f, 1.f };
			LightData mat_diff = { 0.1f, 0.1f, 0.1f, 1.f };
			LightData mat_spec = { 1.f, 1.f, 1.f, 1.f };
			LightData mat_emis = { 0.f, 0.f, 0.f, 0.f };
			GLfloat mat_shine = 0.f;
			Init(mat_amb, mat_diff, mat_spec, mat_emis, mat_shine);
			break;
		}
		case GP3D::Material::METAL:
		{
			LightData mat_amb = { 0.1f, 0.1f, 0.1f, 1.f };
			LightData mat_diff = { 0.1f, 0.1f, 0.1f, 1.f };
			LightData mat_spec = { 0.4f, 0.4f, 0.4f, 1.f };
			LightData mat_emis = { 0.f, 0.f, 0.f, 0.f };
			GLfloat mat_shine = 0.f;
			Init(mat_amb, mat_diff, mat_spec, mat_emis, mat_shine);
			break;
		}
		case GP3D::Material::GLOW:
		{
			LightData mat_amb = { 0.2f, 0.2f, 0.2f, 1.f };
			LightData mat_diff = { 0.5f, 0.5f, 0.5f, 1.f };
			LightData mat_spec = { 0.2f, 0.2f, 0.2f, 1.f };
			LightData mat_emis = { 0.3f, 0.3f, 0.3f, 0.f };
			GLfloat mat_shine = 10.f;
			Init(mat_amb, mat_diff, mat_spec, mat_emis, mat_shine);
			break;
		}
	

		default:
			break;
		}
	}

	// Initialise a material based on parameter light data
	void Material::Init(LightData& amb, LightData& diff, LightData& spec, LightData& emis, GLfloat shiny)
	{
		CopyLightData(amb, ambient_);
		CopyLightData(diff, diffuse_);
		CopyLightData(emis, emission_);
		CopyLightData(spec, specular_);

		shininess_ = shiny;
	}

	// Apply material using GL functions
	void Material::ApplyMaterial()
	{
		glMaterialfv(GL_FRONT, GL_AMBIENT, ambient_);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse_);
		glMaterialfv(GL_FRONT, GL_SPECULAR, specular_);
		glMaterialfv(GL_FRONT, GL_SHININESS, &shininess_);
		glMaterialfv(GL_FRONT, GL_EMISSION, emission_);
	}

	// Copy light data "data" into light data "destination"
	void Material::CopyLightData(LightData data, LightData& destination)
	{
		for (int i = 0; i < k_light_data_max_; i++)
		{
			destination[i] = data[i];
		}
	}
}