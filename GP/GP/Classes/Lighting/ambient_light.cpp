#include "ambient_light.h"

namespace GP3D
{
	AmbientLight::AmbientLight()
	{
	}

	AmbientLight::~AmbientLight()
	{
	}

	void AmbientLight::Init(GLenum light, LightData& amb_data)
	{		
		Light::Init(light);		// Call parent class init

		light_type_ = AMBIENT;

		CopyLightData(amb_data, ambient_);

		diffuse_[0] = 0.0f;
		diffuse_[1] = 0.0f;
		diffuse_[2] = 0.0f;
		diffuse_[3] = 1.0f;

		glLightfv(GL_light_, GL_AMBIENT, ambient_);
		glLightfv(GL_light_, GL_DIFFUSE, diffuse_);
		glEnable(GL_light_);
	}

	void AmbientLight::Update(float dt)
	{
	}
}