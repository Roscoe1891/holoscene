#include "light.h"

namespace GP3D
{
	Light::Light() : k_light_data_max_(4)
	{
	}

	Light::~Light()
	{
	}

	void Light::Init(GLenum light)
	{
		GL_light_ = light;
	}

	// Param in light data "data" to be copied into light data "destination"
	void Light::CopyLightData(LightData data, LightData& destination)
	{
		for (int i = 0; i < k_light_data_max_; i++)
		{
			destination[i] = data[i];
		}
	}

	// Param in new ambient light data
	void Light::SetAmbient(LightData& amb)
	{
		CopyLightData(amb, ambient_);
		glLightfv(GL_light_, GL_AMBIENT, ambient_);
	}
	
	// Param in new diffuse light data
	void Light::SetDiffuse(LightData& diff) 
	{ 
		CopyLightData(diff, diffuse_); 
		glLightfv(GL_light_, GL_DIFFUSE, diffuse_);
	}


}

