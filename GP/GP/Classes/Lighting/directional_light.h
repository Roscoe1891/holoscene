#ifndef _DIRECTIONAL_LIGHT_H
#define _DIRECTIONAL_LIGHT_H

#include "light.h"
#include "maths/vector3.h"

namespace GP3D
{ 
	class DirectionalLight : public Light
	{
	public:
		DirectionalLight();
		~DirectionalLight();
		void Init(GLenum light, LightData& diff_data, LightData& spec_data, Vector3 pos);
		void SetPosition(LightData& pos_data);
		void SetPosition(Vector3 pos);

		virtual void Update(float dt);

		inline LightData& position() { return position_; }
		inline LightData& specular() { return specular_; }
		
	private:
		LightData specular_;
		LightData position_;
	};

}

#endif // !_DIRECTIONAL_LIGHT_H
