#include "point_light.h"

namespace GP3D
{
	PointLight::PointLight()
	{
	}

	PointLight::~PointLight()
	{
	}

	void PointLight::Init(GLenum light, ShapeFactory* shape_factory, LightData& amb_data, LightData& diff_data, LightData& spec_data, Vector3 pos, Vector3 att)
	{
		Light::Init(light);

		light_type_ = POINT;

		CopyLightData(amb_data, ambient_);
		CopyLightData(diff_data, diffuse_);
		CopyLightData(spec_data, specular_);
		SetPosition(pos);
		attenuation_ = att;

		glLightfv(GL_light_, GL_AMBIENT, ambient_);
		glLightfv(GL_light_, GL_DIFFUSE, diffuse_);
		glLightfv(GL_light_, GL_SPECULAR, specular_);
		glLightfv(GL_light_, GL_POSITION, position_);

		glLightf(GL_light_, GL_CONSTANT_ATTENUATION, attenuation_.x());
		glLightf(GL_light_, GL_LINEAR_ATTENUATION, attenuation_.y());
		glLightf(GL_light_, GL_QUADRATIC_ATTENUATION, attenuation_.z());

		shape_factory->CreateShape(&sphere_, ShapeData::SPHERE, 12, pos, NULL, NULL);

	}

	void PointLight::Update(float dt)
	{

	}

	void PointLight::SetPosition(LightData& pos_data)
	{
		CopyLightData(pos_data, position_);
	}

	// Param in new position
	void PointLight::SetPosition(Vector3 pos)
	{
		position_[0] = (GLfloat)pos.x();
		position_[1] = (GLfloat)pos.y();
		position_[2] = (GLfloat)pos.z();
		position_[3] = 1.0f;
	}

	// Light values to be applied at Render time
	void PointLight::Render()
	{
		glLightfv(GL_light_, GL_AMBIENT, ambient_);
		glLightfv(GL_light_, GL_DIFFUSE, diffuse_);
		glLightfv(GL_light_, GL_SPECULAR, specular_);
		glLightfv(GL_light_, GL_POSITION, position_);

		glLightf(GL_light_, GL_CONSTANT_ATTENUATION, attenuation_.x());
		glLightf(GL_light_, GL_LINEAR_ATTENUATION, attenuation_.y());
		glLightf(GL_light_, GL_QUADRATIC_ATTENUATION, attenuation_.z());
	}

	// Render a sphere at this light's position
	void PointLight::RenderLight()
	{
		glPushMatrix();
			sphere_.Render();
		glPopMatrix();
	}
}
