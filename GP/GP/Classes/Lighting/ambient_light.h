#ifndef _AMBIENT_LIGHT_H
#define _AMBIENT_LIGHT_H

#include "light.h"

namespace GP3D
{
	class AmbientLight : public Light
	{
	public:
		AmbientLight();
		virtual~AmbientLight();
		void Init(GLenum light, LightData& amb_data);
		virtual void Update(float dt);

	private:

	};
}

#endif