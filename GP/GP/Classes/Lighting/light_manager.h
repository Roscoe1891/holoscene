#ifndef _LIGHT_MANAGER_H
#define _LIGHT_MANAGER_H

#include "point_light.h"

// Light Manager contains and controls the lights in the scene

namespace GP3D
{

#define MAX_LIGHTS 3

	class LightManager
	{
	public:
		enum LightType{ BLUE, RED, FLAT };

		LightManager();
		~LightManager();
		
		void Init(ShapeFactory* shape_factory);
		void SwitchLightMode();
		void Update(float& dt);
		void RenderLight();
		void Render();

		// param in flashing value
		inline void set_flashing(bool new_value) { flashing_ = new_value; }
		// return flashing
		inline bool flashing() const { return flashing_; }

	private:
		PointLight lights_[MAX_LIGHTS];		// container for lights
		LightType light_type_;				// current ligthing type
		bool flashing_;						// return true if lights are flashing
		float flash_interval_;				// interval between light change during flashing
		float flash_timer_;					// timer for flashing

	};
}

#endif