/*
    TokenStream - Used to return blocks of text in a file.
	By Allen Sherrod and Wendy Jones
*/

#ifndef _TOKEN_STREAM_H_
#define _TOKEN_STREAM_H_

class TokenStream
{
   public:
      TokenStream( );

      void ResetStream( );

      void SetTokenStream( char* data );
      bool GetNextToken( std::string* buffer, char* delimiters, int totalDelimiters );
      bool MoveToNextLine( std::string *buffer );

   private:
      int start_index_, end_index_;
      std::string data_;
};

#endif