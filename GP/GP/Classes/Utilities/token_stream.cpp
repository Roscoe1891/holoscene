/*
    TokenStream - Used to return blocks of text in a file.
	By Allen Sherrod and Wendy Jones
*/

#include <string>
#include "token_stream.h"

bool isValidIdentifier( char c )
{
    // Ascii from ! to ~.
    if( ( int )c > 32 && ( int )c < 127 )
        return true;
      
    return false;
}

bool isValidIdentifier( char c, char* delimiters, int totalDelimiters )
{
    if( delimiters == 0 || totalDelimiters == 0 )
        return isValidIdentifier( c );

    for( int i = 0; i < totalDelimiters; i++ )
    {
        if( c == delimiters[i] )
            return false;
    }
      
    return true;
}


TokenStream::TokenStream( )
{
    ResetStream( );
}


void TokenStream::ResetStream( )
{
    start_index_ = end_index_ = 0;
}


void TokenStream::SetTokenStream( char *data )
{
    ResetStream( );
    data_ = data;
}


bool TokenStream::GetNextToken( std::string* buffer, char* delimiters, int totalDelimiters )
{
    start_index_ = end_index_;

    bool inString = false;
    int length = ( int )data_.length( );

    if( start_index_ >= length - 1 )
        return false;

    while( start_index_ < length && isValidIdentifier( data_[start_index_],
        delimiters, totalDelimiters ) == false )
    {
		start_index_++;
    }

    end_index_ = start_index_ + 1;

    if( data_[start_index_] == '"' )
        inString = !inString;

    if(start_index_ < length )
    {
        while( end_index_ < length && ( isValidIdentifier( data_[end_index_], delimiters,
            totalDelimiters ) || inString == true ) )
        {
            if( data_[end_index_] == '"' )
                inString = !inString;

			end_index_++;
        }

        if( buffer != NULL )
        {
            int size = (end_index_ - start_index_ );
            int index = start_index_;

            buffer->reserve( size + 1 );
            buffer->clear( );

            for( int i = 0; i < size; i++ )
            {
                buffer->push_back( data_[index++] );
            }
        }

        return true;
    }

    return false;
}


bool TokenStream::MoveToNextLine( std::string* buffer )
{
    int length = ( int )data_.length( );

    if( start_index_ < length && end_index_ < length )
    {
		end_index_ = start_index_;

        while( end_index_ < length && ( isValidIdentifier( data_[end_index_] ) ||
            data_[end_index_] == ' ' ) )
        {
			end_index_++;
        }

        if( ( end_index_ - start_index_ ) == 0 )
            return false;

        if( end_index_ - start_index_ >= length )
            return false;

        if( buffer != NULL )
        {
            int size = ( end_index_ - start_index_ );
            int index = start_index_;

            buffer->reserve( size + 1 );
            buffer->clear( );

            for( int i = 0; i < size; i++ )
            {
                buffer->push_back( data_[index++] );
            }
        }
    }
    else
    {
        return false;
    }

	end_index_++;
	start_index_ = end_index_ + 1;

   return true;
}