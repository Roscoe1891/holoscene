#include "wall_decoration.h"

namespace GP3D
{
	WallDecoration::WallDecoration()
	{
	}

	WallDecoration::~WallDecoration()
	{
	}

	void WallDecoration::Init(ShapeFactory* shape_factory, GLuint texture, Vector3 origin, float rotation)
	{
		texture_translate_speed_ = 0.1f;

		Material mat_matt;
		mat_matt.Init(Material::METAL);

		Material mat_glow;
		mat_glow.Init(Material::GLOW);

		shape_factory->CreateShape(&decoration_, ShapeData::PLANE, 5, Vector3(0.f, 0.f, 0.f), &mat_glow, texture);
		decoration_.transform()->set_scale_x(0.2f);

		for (int i = 0; i < 4; i++)
		{
			shape_factory->CreateShape(&frame_[i], ShapeData::CUBE, 5, Vector3(0.f, 0.f, 0.f), &mat_matt);
		}

		// Set Transforms *****************************************************************************

		transform_.set_translation(origin);
		transform_.set_rotation_y(rotation);

		// TOP
		frame_[0].transform()->set_translation_y(2.55f);
		frame_[0].transform()->set_translation_z(-1.5f);

		frame_[0].transform()->set_scale_x(0.2f);
		frame_[0].transform()->set_scale_y(0.02f);
		frame_[0].transform()->set_scale_z(0.02f);

		// BOTTOM
		frame_[1].transform()->set_translation_y(-2.55f);
		frame_[1].transform()->set_translation_z(-1.5f);

		frame_[1].transform()->set_scale_x(0.2f);
		frame_[1].transform()->set_scale_y(0.02f);
		frame_[1].transform()->set_scale_z(0.02f);

		// LEFT
		frame_[2].transform()->set_translation_x(-0.55f);
		frame_[2].transform()->set_translation_z(-1.5f);

		frame_[2].transform()->set_scale_x(0.02f);
		frame_[2].transform()->set_scale_y(1.04f);
		frame_[2].transform()->set_scale_z(0.02f);

		// RIGHT
		frame_[3].transform()->set_translation_x(0.55f);
		frame_[3].transform()->set_translation_z(-1.5f);

		frame_[3].transform()->set_scale_x(0.02f);
		frame_[3].transform()->set_scale_y(1.04f);
		frame_[3].transform()->set_scale_z(0.02f);

	}

	void WallDecoration::Update(float dt)
	{
		texture_translate_ += texture_translate_speed_ * dt;
	}
	
	void WallDecoration::Render()
	{
		glPushMatrix();

			transform_.ApplyTransform();

			glMatrixMode(GL_TEXTURE);	// use texture matrix
			
			glPushMatrix();
				glTranslatef(0.f, texture_translate_, 0.f);	// translate small amount
		
			glMatrixMode(GL_MODELVIEW); // return to model matrix
			glColor3f(1.0f, 1.0f, 1.0f);
			decoration_.Render();

			glMatrixMode(GL_TEXTURE);
			glPopMatrix();

			glMatrixMode(GL_MODELVIEW);
			for (int i = 0; i < 4; i++)
			{
				glColor3f(0.1f, 0.1f, 0.1f);
				frame_[i].Render();
			}

		glPopMatrix();
	}


}