#include "dance_floor.h"

namespace GP3D
{
	DanceFloor::DanceFloor()
	{
	}

	DanceFloor::~DanceFloor()
	{
	}

	void DanceFloor::Init(ShapeFactory* shape_factory)
	{
		lowering_floor_ = false;
		raising_floor_ = false;
		floor_lowered_ = false;
		floor_raised_ = true;

		move_speed_ = 1.f;

		// Set spacing positions:
		floor_spacing_ = 0.6f;
		base_translation_y_ = 0.f;
		lv1_translation_y_ = 0.f;
		lv2_translation_y_ = 0.f;

		// Create materials
		Material mat;
		mat.Init(Material::MATT);

		Material floor_space_mat;
		floor_space_mat.Init(Material::GLOW);

		// Create Shapes

		shape_factory->CreateShape(&floor_base_, ShapeData::CUBE, 10, Vector3(0.0f, 0.0f, 0.0f), &mat);

		for (int i = 0; i < 4; i++)
		{
			shape_factory->CreateShape(&floor_lv1[i], ShapeData::CUBE, 10, Vector3(0.0f, 0.0f, 0.0f), &mat);
			shape_factory->CreateShape(&floor_lv2[i], ShapeData::CUBE, 10, Vector3(0.0f, 0.0f, 0.0f), &mat);
			shape_factory->CreateShape(&floor_lv3[i], ShapeData::CUBE, 10, Vector3(0.0f, 0.0f, 0.0f), &mat);

			shape_factory->CreateShape(&floor_lv1_space[i], ShapeData::PLANE, 10, Vector3(0.0f, 0.0f, 0.0f), &floor_space_mat);
			shape_factory->CreateShape(&floor_lv2_space[i], ShapeData::PLANE, 10, Vector3(0.0f, 0.0f, 0.0f), &floor_space_mat);
			shape_factory->CreateShape(&floor_lv3_space[i], ShapeData::PLANE, 10, Vector3(0.0f, 0.0f, 0.0f), &floor_space_mat);
		}

		// Set Transforms ******************************************************************************************

		// Floor Base
		floor_base_.transform()->set_translation_y(-4.1f);
		//floor_base_.transform()->set_rotation_x(-90.f);
		floor_base_.transform()->set_scale_x(1);
		floor_base_.transform()->set_scale_y(0.02);

		// ************************************************
		// Floor Level 1
		// 0 **********************************************
		floor_lv1[0].transform()->set_rotation_y(90.f);

		floor_lv1[0].transform()->set_translation_z(5.5f);
		floor_lv1[0].transform()->set_translation_y(-4.1f);

		floor_lv1[0].transform()->set_scale_x(0.1f);
		floor_lv1[0].transform()->set_scale_y(0.02f);

		// 1 **********************************************
		floor_lv1[1].transform()->set_rotation_y(90.f);

		floor_lv1[1].transform()->set_translation_z(-5.5f);
		floor_lv1[1].transform()->set_translation_y(-4.1f);

		floor_lv1[1].transform()->set_scale_x(0.1f);
		floor_lv1[1].transform()->set_scale_y(0.02f);

		// 2 **********************************************
		floor_lv1[2].transform()->set_translation_x(-5.5f);
		floor_lv1[2].transform()->set_translation_y(-4.1f);

		floor_lv1[2].transform()->set_scale_x(0.1f);
		floor_lv1[2].transform()->set_scale_y(0.02f);
		floor_lv1[2].transform()->set_scale_z(1.2f);

		// 3 **********************************************
		floor_lv1[3].transform()->set_translation_x(5.5f);
		floor_lv1[3].transform()->set_translation_y(-4.1f);

		floor_lv1[3].transform()->set_scale_x(0.1f);
		floor_lv1[3].transform()->set_scale_y(0.02f);
		floor_lv1[3].transform()->set_scale_z(1.2f);


		// ************************************************
		// Floor Spaces Level 1
		// 0 **********************************************
		floor_lv1_space[0].transform()->set_translation_z(-1.2f);
		floor_lv1_space[0].transform()->set_translation_y(-4.7f);

		floor_lv1_space[0].transform()->set_scale_x(1.04f);
		floor_lv1_space[0].transform()->set_scale_y(0.1f);

		// 1 **********************************************
		floor_lv1_space[1].transform()->set_rotation_y(180.f);

		floor_lv1_space[1].transform()->set_translation_z(1.2f);
		floor_lv1_space[1].transform()->set_translation_y(-4.7f);

		floor_lv1_space[1].transform()->set_scale_x(1.04f);
		floor_lv1_space[1].transform()->set_scale_y(0.1f);

		// 2 **********************************************
		floor_lv1_space[2].transform()->set_rotation_y(-90.f);

		floor_lv1_space[2].transform()->set_translation_x(1.2f);
		floor_lv1_space[2].transform()->set_translation_y(-4.7f);

		floor_lv1_space[2].transform()->set_scale_x(1.04f);
		floor_lv1_space[2].transform()->set_scale_y(0.1f);

		// 3 **********************************************
		floor_lv1_space[3].transform()->set_rotation_y(90.f);

		floor_lv1_space[3].transform()->set_translation_x(-1.2f);
		floor_lv1_space[3].transform()->set_translation_y(-4.7f);

		floor_lv1_space[3].transform()->set_scale_x(1.04f);
		floor_lv1_space[3].transform()->set_scale_y(0.1f);


		// ************************************************
		// Floor Level 2
		// 0 **********************************************
		floor_lv2[0].transform()->set_rotation_y(90.f);

		floor_lv2[0].transform()->set_translation_z(6.5f);
		floor_lv2[0].transform()->set_translation_y(-4.1f);

		floor_lv2[0].transform()->set_scale_x(0.1f);
		floor_lv2[0].transform()->set_scale_y(0.02f);
		floor_lv2[0].transform()->set_scale_z(1.2f);

		// 1 **********************************************
		floor_lv2[1].transform()->set_rotation_y(90.f);

		floor_lv2[1].transform()->set_translation_z(-6.5f);
		floor_lv2[1].transform()->set_translation_y(-4.1f);

		floor_lv2[1].transform()->set_scale_x(0.1f);
		floor_lv2[1].transform()->set_scale_y(0.02f);
		floor_lv2[1].transform()->set_scale_z(1.2f);

		// 2 **********************************************
		floor_lv2[2].transform()->set_translation_x(-6.5f);
		floor_lv2[2].transform()->set_translation_y(-4.1f);

		floor_lv2[2].transform()->set_scale_x(0.1f);
		floor_lv2[2].transform()->set_scale_y(0.02f);
		floor_lv2[2].transform()->set_scale_z(1.4f);

		// 3 **********************************************
		floor_lv2[3].transform()->set_translation_x(6.5f);
		floor_lv2[3].transform()->set_translation_y(-4.1f);

		floor_lv2[3].transform()->set_scale_x(0.1f);
		floor_lv2[3].transform()->set_scale_y(0.02f);
		floor_lv2[3].transform()->set_scale_z(1.4f);


		// ************************************************
		// Floor Spaces Level 2
		// 0 **********************************************
		floor_lv2_space[0].transform()->set_translation_z(-2.2f);
		floor_lv2_space[0].transform()->set_translation_y(-4.7f);

		floor_lv2_space[0].transform()->set_scale_x(1.24f);
		floor_lv2_space[0].transform()->set_scale_y(0.1f);

		// 1 **********************************************
		floor_lv2_space[1].transform()->set_rotation_y(180.f);

		floor_lv2_space[1].transform()->set_translation_z(2.2f);
		floor_lv2_space[1].transform()->set_translation_y(-4.7f);

		floor_lv2_space[1].transform()->set_scale_x(1.24f);
		floor_lv2_space[1].transform()->set_scale_y(0.1f);

		// 2 **********************************************
		floor_lv2_space[2].transform()->set_rotation_y(-90.f);

		floor_lv2_space[2].transform()->set_translation_x(2.2f);
		floor_lv2_space[2].transform()->set_translation_y(-4.7f);

		floor_lv2_space[2].transform()->set_scale_x(1.24f);
		floor_lv2_space[2].transform()->set_scale_y(0.1f);

		// 3 **********************************************
		floor_lv2_space[3].transform()->set_rotation_y(90.f);

		floor_lv2_space[3].transform()->set_translation_x(-2.2f);
		floor_lv2_space[3].transform()->set_translation_y(-4.7f);

		floor_lv2_space[3].transform()->set_scale_x(1.24f);
		floor_lv2_space[3].transform()->set_scale_y(0.1f);


		// ************************************************
		// Floor Level 3
		// 0 **********************************************
		floor_lv3[0].transform()->set_rotation_y(90.f);

		floor_lv3[0].transform()->set_translation_z(7.5f);
		floor_lv3[0].transform()->set_translation_y(-4.1f);

		floor_lv3[0].transform()->set_scale_x(0.1f);
		floor_lv3[0].transform()->set_scale_y(0.02f);
		floor_lv3[0].transform()->set_scale_z(1.4f);

		// 1 **********************************************
		floor_lv3[1].transform()->set_rotation_y(90.f);

		floor_lv3[1].transform()->set_translation_z(-7.5f);
		floor_lv3[1].transform()->set_translation_y(-4.1f);

		floor_lv3[1].transform()->set_scale_x(0.1f);
		floor_lv3[1].transform()->set_scale_y(0.02f);
		floor_lv3[1].transform()->set_scale_z(1.4f);

		// 2 **********************************************
		floor_lv3[2].transform()->set_translation_x(-7.5f);
		floor_lv3[2].transform()->set_translation_y(-4.1f);

		floor_lv3[2].transform()->set_scale_x(0.1f);
		floor_lv3[2].transform()->set_scale_y(0.02f);
		floor_lv3[2].transform()->set_scale_z(1.6f);

		// 3 **********************************************
		floor_lv3[3].transform()->set_translation_x(7.5f);
		floor_lv3[3].transform()->set_translation_y(-4.1f);

		floor_lv3[3].transform()->set_scale_x(0.1f);
		floor_lv3[3].transform()->set_scale_y(0.02f);
		floor_lv3[3].transform()->set_scale_z(1.6f);


		// ************************************************
		// Floor Spaces Level 3
		// 0 **********************************************
		floor_lv3_space[0].transform()->set_translation_z(-3.2f);
		floor_lv3_space[0].transform()->set_translation_y(-4.7f);

		floor_lv3_space[0].transform()->set_scale_x(1.44f);
		floor_lv3_space[0].transform()->set_scale_y(0.1f);

		// 1 **********************************************
		floor_lv3_space[1].transform()->set_rotation_y(180.f);

		floor_lv3_space[1].transform()->set_translation_z(3.2f);
		floor_lv3_space[1].transform()->set_translation_y(-4.7f);

		floor_lv3_space[1].transform()->set_scale_x(1.44f);
		floor_lv3_space[1].transform()->set_scale_y(0.1f);

		// 2 **********************************************
		floor_lv3_space[2].transform()->set_rotation_y(-90.f);

		floor_lv3_space[2].transform()->set_translation_x(3.2f);
		floor_lv3_space[2].transform()->set_translation_y(-4.7f);

		floor_lv3_space[2].transform()->set_scale_x(1.44f);
		floor_lv3_space[2].transform()->set_scale_y(0.1f);

		// 3 **********************************************
		floor_lv3_space[3].transform()->set_rotation_y(90.f);

		floor_lv3_space[3].transform()->set_translation_x(-3.2f);
		floor_lv3_space[3].transform()->set_translation_y(-4.7f);

		floor_lv3_space[3].transform()->set_scale_x(1.44f);
		floor_lv3_space[3].transform()->set_scale_y(0.1f);
	}

	void DanceFloor::Update(float& dt)
	{
		if (lowering_floor_)
		{
			// lower floor
			LowerFloor(dt);
		}
		else if (raising_floor_)
		{
			// raise floor
			RaiseFloor(dt);
		}
	}

	void DanceFloor::LowerFloor(float& dt)
	{
		if (floor_raised_) floor_raised_ = false;

		if (lv2_translation_y_ > -floor_spacing_)
		{
			lv2_translation_y_ -= (move_speed_ * dt);
		} 
		else if (lv1_translation_y_ > -floor_spacing_)
		{
			lv1_translation_y_ -= (move_speed_ * dt);
		}
		else if (base_translation_y_ > -floor_spacing_)
		{
			base_translation_y_ -= (move_speed_ * dt);
		}
		else
		{
			lowering_floor_ = false;
			floor_lowered_ = true;
		}
	}

	void DanceFloor::RaiseFloor(float& dt)
	{
		if (floor_lowered_) floor_lowered_ = false;

		if (base_translation_y_ < 0)
		{
			base_translation_y_ += (move_speed_ * dt);
		}
		else if (lv1_translation_y_ < 0)
		{
			lv1_translation_y_ += (move_speed_ * dt);
		}
		else if (lv2_translation_y_ < 0)
		{
			lv2_translation_y_ += (move_speed_ * dt);
		}
		else
		{
			raising_floor_ = false;
			floor_raised_ = true;
		}
	}

	void DanceFloor::Render()
	{
		glPushMatrix();
		{
			for (int i = 0; i < 4; i++)
			{
				glPushMatrix();
					glColor3f(0.25, 0.25, 0.25);
					floor_lv3[i].Render();
				glPopMatrix();

				glPushMatrix();
					glColor3f(1.0, 0.0, 1.0);
					floor_lv3_space[i].Render();
				glPopMatrix();
			}
			
			glPushMatrix();
			{
				glTranslatef(0.f, lv2_translation_y_, 0.f);
				for (int i = 0; i < 4; i++)
				{
					glPushMatrix();
						glColor3f(0.25, 0.25, 0.25);
						floor_lv2[i].Render();
					glPopMatrix();

					glPushMatrix();
						glColor3f(1.0, 0.0, 1.0);
						floor_lv2_space[i].Render();
					glPopMatrix();
				}

				glPushMatrix();
				{
					glTranslatef(0.f, lv1_translation_y_, 0.f);

					for (int i = 0; i < 4; i++)
					{
						glPushMatrix();
							glColor3f(0.25, 0.25, 0.25);
							floor_lv1[i].Render();
						glPopMatrix();

						glPushMatrix();
							glColor3f(1.0, 0.0, 1.0);
							floor_lv1_space[i].Render();
						glPopMatrix();
					}

					glPushMatrix();
					{
						glTranslatef(0.f, base_translation_y_, 0.f);

						glColor3f(0.25, 0.25, 0.25);
						floor_base_.Render();
					}
					glPopMatrix();
				
				}
				glPopMatrix();
			}
			glPopMatrix();
		}
		glPopMatrix();
		glColor3f(1.0, 1.0, 1.0);
	}

}
