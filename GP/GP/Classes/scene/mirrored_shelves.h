#ifndef _MIRRORED_SHEVES_H
#define _MIRRORED_SHEVES_H

#include "Geometry\shape_factory.h"
#include "model\Model.h"

namespace GP3D
{
#define MAX_SHELVES 3

	class MirroredShelves
	{

	public:
		MirroredShelves();
		~MirroredShelves();
		void Init(ShapeFactory* shape_factory, Model* bottle, GLuint shelf_texture);
		void Render();

	private:
		Model* bottle_;
		Shape mirror_surface_;
		Shape shelves_[MAX_SHELVES];
	};
}

#endif