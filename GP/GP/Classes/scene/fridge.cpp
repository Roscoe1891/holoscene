#include "fridge.h"

namespace GP3D
{
	Fridge::Fridge() : cabinet_(NULL), can_(NULL)
	{
	}

	Fridge::~Fridge()
	{
	}

	void Fridge::Init(ShapeFactory* shape_factory, Model* cabinet, Model* can)
	{
		cabinet_ = cabinet;
		cabinet_->transform()->set_scale(Vector3(0.1f, 0.1f, 0.1f));

		can_ = can;
		can_->transform()->set_scale(Vector3(0.1f, 0.1f, 0.1f));
		can_->transform()->set_translation(Vector3(-1.01f, -0.6f, 1.2f));

		shape_factory->CreateShape(&panel_, ShapeData::PLANE, 1);
		panel_.transform()->set_translation(Vector3(0.16f, 1.1f, 1.17f));
		panel_.transform()->set_scale(Vector3(1.7f, 2.1f, 1.f));
	}

	void Fridge::Render()
	{
		glPushMatrix();

		transform_.ApplyTransform();

		glPushMatrix();
		for (int i = 0; i < 3; ++i)
		{
			glTranslatef(2.11f, 0.f, 0.f);
			cabinet_->Render(true);

			glPushMatrix();
			for (int y = 0; y < 3; ++y)
			{
				glTranslatef(0.f, 0.7f, 0.f);
				glPushMatrix();
				for (int x = 0; x < 5; ++x)
				{
					glTranslatef(0.34f, 0.f, 0.f);
					can_->Render(true);
				}
				glPopMatrix();
			}
			glPopMatrix();

		}
			
		glPopMatrix();

			glEnable(GL_BLEND);  //enable blending
		
			glColor4f(0, 0.5, 0.5, 0.25); // set colour and alpha
			
			glPushMatrix();
			for (int i = 0; i < 3; ++i)
			{
				glTranslatef(2.11f, 0.f, 0.f);
				panel_.Render();
			}
			glPopMatrix();

			glDisable(GL_BLEND);  //disable blending

		glPopMatrix();

	}

}