#ifndef _TABLE_H
#define _TABLE_H

#include "Geometry\shape_factory.h"

namespace GP3D
{

#define MAX_COMPONENTS 7

	class Table
	{
	public:
		enum TableComponents { SHADOW, BASE_CYLINDER, BASE_DISC, STAND_CYLINDER, SURFACE_DISC_LOW, SURFACE_CYLINDER, SURFACE_DISC_HIGH };
		
		Table();
		~Table();

		void Init(ShapeFactory* shape_factory, GLuint shadow_texture, GLuint texture, Vector3 origin);
		void Render();

		inline Transform* transform() { return &transform_; }

	private:
		Shape components_[MAX_COMPONENTS];
		Transform transform_;


	};

}

#endif