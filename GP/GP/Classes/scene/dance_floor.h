#ifndef _DANCE_FLOOR_H
#define _DANCE_FLOOR_H

#include "Geometry\shape_factory.h"

namespace GP3D
{
	class DanceFloor
	{
	public:
		DanceFloor();
		~DanceFloor();

		void Init(ShapeFactory* shape_factory);
		void Update(float& dt);
		void Render();

		inline void set_lowering_floor(bool new_value) { lowering_floor_ = new_value; }
		inline void set_raising_floor(bool new_value) { raising_floor_ = new_value; }

		inline bool floor_lowered() const { return floor_lowered_; }
		inline bool floor_raised() const { return floor_raised_; }

	private:
		void LowerFloor(float& dt);
		void RaiseFloor(float& dt);

		Shape floor_base_;									// Base of the floor 
		Shape floor_lv1[4];									// lv1 of the floor
		Shape floor_lv2[4];									// lv2 of the floor
		Shape floor_lv3[4];									// lv3 of the floor (does not move)
		Shape floor_lv1_space[4];							// lv1 of the floor
		Shape floor_lv2_space[4];							// lv2 of the floor
		Shape floor_lv3_space[4];							// lv3 of the floor (does not move)

		float floor_spacing_;
		float base_translation_y_;
		float lv1_translation_y_;
		float lv2_translation_y_;

		float move_speed_;									// speed at which levels move

		bool lowering_floor_;								// true if floor is currently lowering
		bool floor_lowered_;								// true if floor is fully lowered
		bool raising_floor_;								// true if floor is currently rising
		bool floor_raised_;									// true if floor is fully raised


	};

}

#endif // !_DANCE_FLOOR_H