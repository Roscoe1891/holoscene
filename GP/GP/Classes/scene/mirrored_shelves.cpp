#include "mirrored_shelves.h"

namespace GP3D
{
	MirroredShelves::MirroredShelves() : bottle_(NULL)
	{
	}

	MirroredShelves::~MirroredShelves()
	{
	}

	void MirroredShelves::Init(ShapeFactory* shape_factory, Model* bottle, GLuint shelf_texture)
	{
		bottle_ = bottle;

		shape_factory->CreateShape(&mirror_surface_, ShapeData::PLANE, 6);
		mirror_surface_.transform()->set_translation(Vector3(0.f, 3.f, -17.f));

		Material shelf_mat;
		shelf_mat.Init(Material::MATT);

		int spacing = 2;
		for (int i = 0; i < MAX_SHELVES; i++)
		{
			shape_factory->CreateShape(&shelves_[i], ShapeData::CUBE, 1, Vector3(0.f, 0.f, 0.f), &shelf_mat, shelf_texture);
			shelves_[i].transform()->set_translation(Vector3(0.f, -0.1f +(i * spacing), -18.5f));
			shelves_[i].transform()->set_scale(Vector3(6.f, 0.2f, 1.f));
		}
	}

	void MirroredShelves::Render()
	{
		glPushMatrix();
		{
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);	// disable writing to frame buffer
			glEnable(GL_STENCIL_TEST);								// enable stencil testing
			glStencilFunc(GL_ALWAYS, 1, 1);							// always pass
			glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);				// define stencil operation
			glDisable(GL_DEPTH_TEST);								// disable depth test
			mirror_surface_.Render();

			glEnable(GL_DEPTH_TEST);								// enable depth test
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);		// enable writing to frame buffer
			glStencilFunc(GL_EQUAL, 1, 1);							// only pass if stencil value == 1
			glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);					// don't change stencil values

			// DRAW REFLECTED OBJECTS
			glPushMatrix();
			{
				glScalef(1.0, 1.0f, -1.0);							// flip vertically

				glPushMatrix();
					glTranslatef(0.f, 0.f, 38.f);

					for (int i = 0; i < MAX_SHELVES; i++)
					{
						shelves_[i].Render();
					}
				glPopMatrix();

				glTranslatef(-3.7f, 6.f, 19.2f);						// translate obj to drawing plane

				for (int y = 0; y < MAX_SHELVES; y++)
				{
					glTranslatef(0.f, -2.f, 0.f);

					glPushMatrix();
					glPolygonMode(GL_BACK, GL_FILL);
					float reflected_xpos = 1.f;
					for (int x = 0; x < 6; x++)
					{
						glTranslatef(reflected_xpos, 0.f, 0.f);
						bottle_->Render(true);
					}
					glPolygonMode(GL_BACK, GL_LINE);

					glPopMatrix();
				}

			}
			glPopMatrix();

			glDisable(GL_STENCIL_TEST);								// done with stencil test
			glEnable(GL_BLEND);										// combine drawing plane and rendered object
			glDisable(GL_LIGHTING);									// disable lighting
			glColor4f(0.3f, 0.6f, 0.9f, 0.5f);
			mirror_surface_.Render();
			glEnable(GL_LIGHTING);
			glDisable(GL_BLEND);

			// DRAW REAL OBJECTS
			glPushMatrix();
			{
				glPushMatrix();
				glTranslatef(-3.7f, 6.f, -18.8f);
				glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

				for (int y = 0; y < MAX_SHELVES; y++)
				{
					glTranslatef(0.f, -2.f, 0.f);
					glPushMatrix();
					float xpos = 1.f;
					for (int x = 0; x < 6; x++)
					{
						glTranslatef(xpos, 0.f, 0.f);
						bottle_->Render(true);
					}
					glPopMatrix();
				}
				glPopMatrix();

				glPushMatrix();
				//glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

				for (int i = 0; i < MAX_SHELVES; i++)
				{
					shelves_[i].Render();
				}
				glPopMatrix();

			}
			glPopMatrix();
		}
		glPopMatrix();

		glColor4f(1.f, 1.f, 1.f, 1.f);
	}
}
