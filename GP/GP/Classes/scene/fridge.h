#ifndef _FRIDGE_H
#define _FRIDGE_H

#include "model\Model.h"
#include "Geometry\shape_factory.h"

namespace GP3D
{

	class Fridge
	{

	public:
		Fridge();
		~Fridge();

		void Init(ShapeFactory* shape_factory, Model* cabinet, Model* can);
		void Render();

		inline Transform* transform() { return &transform_; }

	private:
		Transform transform_;
		Model* cabinet_;
		Model* can_;
		Shape panel_;

	};
}

#endif

