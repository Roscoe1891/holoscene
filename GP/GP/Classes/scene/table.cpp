#include "table.h"

namespace GP3D
{
	Table::Table()
	{
	}

	Table::~Table()
	{
	}

	void Table::Init(ShapeFactory* shape_factory, GLuint shadow_texture, GLuint texture, Vector3 origin)
	{
		shape_factory->CreateShape(&components_[SHADOW], ShapeData::PLANE, 3, origin, NULL, shadow_texture);
		components_[SHADOW].transform()->set_rotation_x(-90.f);
		components_[SHADOW].transform()->set_translation_y(0.3f);

		shape_factory->CreateShape(&components_[BASE_CYLINDER], ShapeData::CYLINDER, 20, origin, NULL, texture);
		components_[BASE_CYLINDER].transform()->set_scale_y(0.1f);

		shape_factory->CreateShape(&components_[BASE_DISC], ShapeData::DISC, 20, origin, NULL, texture);
		components_[BASE_DISC].transform()->set_rotation_x(-90.f);
		components_[BASE_DISC].transform()->set_translation_y(0.2f);

		shape_factory->CreateShape(&components_[STAND_CYLINDER], ShapeData::CYLINDER, 16, origin, NULL, texture);
		components_[STAND_CYLINDER].transform()->set_scale(Vector3(0.2f, 0.5f, 0.2f));
		components_[STAND_CYLINDER].transform()->set_translation_y(1.2f);

		shape_factory->CreateShape(&components_[SURFACE_DISC_LOW], ShapeData::DISC, 24, origin, NULL, texture);
		components_[SURFACE_DISC_LOW].transform()->set_rotation_x(90.f);
		components_[SURFACE_DISC_LOW].transform()->set_translation_y(2.2f);

		shape_factory->CreateShape(&components_[SURFACE_CYLINDER], ShapeData::CYLINDER, 24, origin, NULL, texture);
		components_[SURFACE_CYLINDER].transform()->set_scale_y(0.1f);
		components_[SURFACE_CYLINDER].transform()->set_translation_y(2.4f);

		shape_factory->CreateShape(&components_[SURFACE_DISC_HIGH], ShapeData::DISC, 24, origin, NULL, texture);
		components_[SURFACE_DISC_HIGH].transform()->set_rotation_x(-90.f);
		components_[SURFACE_DISC_HIGH].transform()->set_translation_y(2.6f);
	}

	void Table::Render()
	{	
		glPushMatrix();
		transform_.ApplyTransform();

		// disable lighting
		glDisable(GL_LIGHTING);
		// enable blending
		glEnable(GL_BLEND);
		components_[SHADOW].Render();
		// disable blending
		glDisable(GL_BLEND);
		// enable lighting
		glEnable(GL_LIGHTING);

		components_[BASE_CYLINDER].Render();
		components_[BASE_DISC].Render();
		components_[STAND_CYLINDER].Render();

		glPushMatrix();
			glScalef(1.5, 1.f, 1.5f);
			components_[SURFACE_DISC_LOW].Render();
			components_[SURFACE_CYLINDER].Render();
			components_[SURFACE_DISC_HIGH].Render();
		glPopMatrix();

		glPopMatrix();
	}


}