#ifndef _WALL_DECORATION_H
#define _WALL_DECORATION_H

#include "Maths\Vector3.h"
#include "geometry\shape_factory.h"
#include "geometry\shape.h"
#include "geometry\transform.h"

namespace GP3D
{
	class WallDecoration
	{
	public:
		WallDecoration();
		~WallDecoration();

		void Init(ShapeFactory* shape_factory, GLuint texture, Vector3 origin, float rotation);
		void Update(float dt);
		void Render();

	private:
		Transform transform_;
		float texture_translate_;
		float texture_translate_speed_;

		Shape frame_[4];
		Shape decoration_;
	};
}

#endif