#include "camera_controller.h"

namespace GP3D
{
	CameraController::CameraController() : move_speed_(10), rotate_speed_(120)
	{
	}

	CameraController::~CameraController()
	{
	}

	// Initialise
	void CameraController::Init(Camera* cam)
	{
		camera_ = cam;
	}

	// Update
	void CameraController::Update(float& dTime)
	{
		update_vectors_ = FALSE;								// no controller updates received

		new_position_ = camera_->position();					// update position
		new_orientation_ = camera_->orientation();				// update rotation

		ControllerUpdate(dTime);								// controller specific update

		if (update_vectors_)									// if cam has been moved or rotated
		{
			camera_->UpdateVectors();							// update camera
		}
	}

	// Movement Functions
	void CameraController::MoveUp(float& dTime)
	{
		new_position_.Add(camera_->up(), move_speed_ * dTime);
	}

	void CameraController::MoveDown(float& dTime)
	{
		new_position_.Subtract(camera_->up(), move_speed_ * dTime);
	}

	void CameraController::MoveRight(float& dTime)
	{
		new_position_.Add(camera_->right(), move_speed_ * dTime);
	}

	void CameraController::MoveLeft(float& dTime)
	{
		new_position_.Subtract(camera_->right(), move_speed_ * dTime);
	}

	void CameraController::MoveForward(float& dTime)
	{
		new_position_.Add(camera_->forward(), move_speed_ * dTime);
	}

	void CameraController::MoveBack(float& dTime)
	{
		new_position_.Subtract(camera_->forward(), move_speed_ * dTime);
	}

	void CameraController::RotateUp(float& dTime)
	{
		new_orientation_.set_x(camera_->orientation().x() + rotate_speed_ * dTime);
	}

	// Rotation Functions
	void CameraController::RotateDown(float& dTime)
	{
		new_orientation_.set_x(camera_->orientation().x() - rotate_speed_ * dTime);
	}

	void CameraController::RotateRight(float& dTime)
	{
		new_orientation_.set_y(camera_->orientation().y() + rotate_speed_ * dTime);
	}

	void CameraController::RotateLeft(float& dTime)
	{
		new_orientation_.set_y(camera_->orientation().y() - rotate_speed_ * dTime);
	}

	// Reset camera position, look at and up
	void CameraController::Reset()
	{
		camera_->Init(Vector3(0.f, 0.f, 6.f), Vector3(0.f, 0.f, 0.f), Vector3(0.f, 1.f, 0.f));
	}
}



