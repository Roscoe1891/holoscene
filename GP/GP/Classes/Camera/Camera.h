#ifndef _CAMERA_H
#define _CAMERA_H

#include <cmath>
#include "maths/math_definitions.h"
#include "maths/vector3.h"

// Camera
// Defines how the scene is viewed

namespace GP3D
{

	class Camera
	{
	public:
		Camera();
		~Camera();

		void Init(Vector3 pos, Vector3 look, Vector3 u);							// Initialise
		void UpdateVectors();														// Update camera values

		inline const Vector3 position() { return position_; }						// Get Camera position
		inline void set_position(Vector3 pos) { position_ = pos; }					// Set Camera position

		inline const Vector3 orientation() { return orientation_; }					// Get Orinetation
		inline void set_orientation(Vector3 orient) { orientation_ = orient; }		// Set Orientation

		inline void set_pitch(float orient) { orientation_.set_x(orient); }			// Set pitch
		inline void set_yaw(float orient) { orientation_.set_y(orient); }			// Set yaw

		inline const Vector3 look_at() { return look_at_; }							// Get Direction camera is looking
		inline const Vector3 up() { return up_; }									// Get Up vector
		inline const Vector3 right() { return right_; }								// Get Right vector
		inline const Vector3 forward() { return forward_; }							// Get Forward vector

	private:
		Vector3 position_;		// World position
		Vector3 orientation_;	// (x = pitch, y = yaw, z = roll)
		Vector3 look_at_;		// Direction camera is looking
		Vector3 up_;			// Up vector
		Vector3 forward_;		// Forward vector
		Vector3 right_;			// Right vector
	};

}

#endif