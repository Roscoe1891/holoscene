#ifndef _CAMERA_CONTROLLER_SECURITY_H
#define _CAMERA_CONTROLLER_SECURITY_H

#include "camera_controller.h"

// Simulates a Security camera which pans back and forth

namespace GP3D
{
	class CameraControllerSecurity : public CameraController
	{
	public:
		CameraControllerSecurity();
		~CameraControllerSecurity();

		void ControllerInit(Camera* cam, Vector3 orientation, float pan_radius);	// Controller Initialiser

	private:
		void virtual ControllerUpdate(float& dTime);								// Controller specific update (defined by inherited class)

		bool moving_right_;			// True if moving right
		float pan_radius_;			// Angle limit for camera panning
		float initial_rotation_;	// Angle the camera starts at
	};
}

#endif // !_CAMERA_CONTROLLER_SECURITY_H
