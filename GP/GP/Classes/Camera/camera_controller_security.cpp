#include "camera_controller_security.h"

namespace GP3D
{
	CameraControllerSecurity::CameraControllerSecurity()
	{
		moving_right_ = true;
	}

	CameraControllerSecurity::~CameraControllerSecurity()
	{
	}

	// Controller Initialiser
	void CameraControllerSecurity::ControllerInit(Camera* cam, Vector3 orientation, float pan_radius)
	{
		Init(cam);
		camera_->set_orientation(orientation);
		initial_rotation_ = orientation.y();
		pan_radius_ = pan_radius;
		moving_right_ = true;
		set_rotate_speed(10.f);
	}

	// Controller specific update (defined by inherited class)
	void CameraControllerSecurity::ControllerUpdate(float& dTime)
	{
		if (pan_radius_ != 0)
		{
			// if rotating left
			if (!moving_right_)
			{
				if (camera_->orientation().y() <= initial_rotation_ - pan_radius_) //  & reached left limit
				{
					moving_right_ = true;	// now rotating right
				}
				else // if not reached limit
				{
					RotateLeft(dTime);	// keep rotating left
				}
			}
			// if moving right
			else
			{
				if (camera_->orientation().y() >= initial_rotation_ + pan_radius_) //  & reached right limit
				{
					moving_right_ = false;	// no longer rotating right
				}
				else // if not reached limit
				{
					RotateRight(dTime);	// keep rotating right
				}
			}
		}
		
		// set camera to its new rotation and update
		camera_->set_orientation(new_orientation_);
		update_vectors_ = TRUE;
	}

}