#include "scene_3D.h"

namespace GP3D
{

	bool Scene3D::CreatePixelFormat(HDC hdc)
	{
		PIXELFORMATDESCRIPTOR pfd = { 0 };
		int pixelformat;

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);	// Set the size of the structure
		pfd.nVersion = 1;							// Always set this to 1
		// Pass in the appropriate OpenGL flags
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.dwLayerMask = PFD_MAIN_PLANE;			// standard mask (this is ignored anyway)
		pfd.iPixelType = PFD_TYPE_RGBA;				// RGB and Alpha pixel typef
		pfd.cColorBits = COLOUR_DEPTH;				// Here we use our #define for the color bits
		pfd.cDepthBits = COLOUR_DEPTH;				// Ignored for RBA
		pfd.cAccumBits = 0;							// nothing for accumulation
		pfd.cStencilBits = COLOUR_DEPTH;			// stencil buffer matches colour buffer

		//Gets a best match on the pixel format as passed in from device
		if ((pixelformat = ChoosePixelFormat(hdc, &pfd)) == false)
		{
			MessageBox(NULL, "ChoosePixelFormat failed", "Error", MB_OK);
			return false;
		}

		//sets the pixel format if its ok. 
		if (SetPixelFormat(hdc, pixelformat, &pfd) == false)
		{
			MessageBox(NULL, "SetPixelFormat failed", "Error", MB_OK);
			return false;
		}

		return true;
	}

	void Scene3D::ResizeGLWindow(int width, int height)// Initialize The GL Window
	{
		if (height == 0)// Prevent A Divide By Zero error
		{
			height = 1;// Make the Height Equal One
		}

		glViewport(0, 0, width, height);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		//calculate angle of view & aspect ratio
		gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 1, 150.0f);

		glMatrixMode(GL_MODELVIEW);// Select The Modelview Matrix
		glLoadIdentity();// Reset The Modelview Matrix
	}

	void Scene3D::InitializeOpenGL(int width, int height)
	{
		hdc_ = GetDC(*hwnd_);//  sets  global HDC

		if (!CreatePixelFormat(hdc_))//  sets  pixel format
			PostQuitMessage(0);

		hrc_ = wglCreateContext(hdc_);	//  creates  rendering context from  hdc
		wglMakeCurrent(hdc_, hrc_);		//	Use this HRC.

		ResizeGLWindow(width, height);	// Setup the Screen
	}

	void Scene3D::Init(HWND* wnd, Input* in)
	{
		hwnd_ = wnd;
		input_ = in;

		GetClientRect(*hwnd_, &screen_rect_);	//get rect into our handy global rect
		InitializeOpenGL(screen_rect_.right, screen_rect_.bottom); // initialise openGL

		//OpenGL settings
		glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
		//glClearColor(0.39f, 0.58f, 93.0f, 1.0f);			// Cornflour Blue Background
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);				// Black Background
		glClearDepth(1.0f);									// Depth Buffer Setup
		glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
		glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
		glEnable(GL_LIGHTING);								// Turn on lighting
		glEnable(GL_COLOR_MATERIAL);
		//glEnable(GL_TEXTURE_2D);										// Enables 2D textures
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);	// Specify Texture calculations
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Specify texture wrap
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// Specify texture wrap
		glClearStencil(0);												// Set how to clear stencil buffer

		// define blend function
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// calculate centres
		client_centre_ = CalcClientCentre(screen_rect_);
		screen_centre_ = CalcScreenCentre(screen_rect_);

		// wireframe not enabled on initialisation
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		wireframe_enabled_ = false;

		// Initialise cameras and controllers
		camera_mode_ = MAIN;

		cameras_[MAIN].Init(Vector3(0.f, 0.f, 20.f), Vector3(0.f, 0.f, 0.f), Vector3(0.f, 1.f, 0.f));
		camera_controller_main_.ControllerInit(&cameras_[MAIN], input_, &client_centre_, VK_UP, VK_DOWN, 0x41, 0x44, 0x57, 0x53, VK_SPACE);
		camera_controllers_[MAIN] = &camera_controller_main_;

		cameras_[SECURITY].Init(Vector3(7.f, 5.f, -16.f), Vector3(0.f, 0.f, 0.f), Vector3(0.f, 1.f, 0.f));
		camera_controller_security_panning_.ControllerInit(&cameras_[SECURITY], Vector3(-20.f, 215.f, 0.f), 10.f);
		camera_controllers_[SECURITY] = &camera_controller_security_panning_;

		cameras_[FIXED].Init(Vector3(0.f, 2.f, -17.f), Vector3(0.f, 0.f, 0.f), Vector3(0.f, 1.f, 0.f));
		camera_controller_security_.ControllerInit(&cameras_[FIXED], Vector3(0, 180.f, 0.f), 0.f);
		camera_controllers_[FIXED] = &camera_controller_security_;

		// load textures
		light_bar_texture_ = SOIL_load_OGL_texture("Textures/lightbar.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
		if (light_bar_texture_ == 0) MessageBox(*hwnd_, "lightbar texture failed to load", "Fail", MB_OK);
	
		wallpaper_texture1_ = SOIL_load_OGL_texture("Textures/swirl_wallpaper.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
		if (wallpaper_texture1_ == 0) MessageBox(*hwnd_, "wallpaper texture 1 failed to load", "Fail", MB_OK);

		wallpaper_texture2_ = SOIL_load_OGL_texture("Textures/stripe_wallpaper.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
		if (wallpaper_texture2_ == 0) MessageBox(*hwnd_, "wallpaper texture 2 failed to load", "Fail", MB_OK);

		wood_texture_ = SOIL_load_OGL_texture("Textures/dark_wood.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
		if (wood_texture_ == 0) MessageBox(*hwnd_, "wood texture texture failed to load", "Fail", MB_OK);

		carpet_texture_ = SOIL_load_OGL_texture("Textures/carpet.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
		if (carpet_texture_ == 0) MessageBox(*hwnd_, "carpet texture texture failed to load", "Fail", MB_OK);

		shadow_texture_ = SOIL_load_OGL_texture("Textures/imposter.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
		if (shadow_texture_ == 0) MessageBox(*hwnd_, "imposter texture texture failed to load", "Fail", MB_OK);

		// load models
		bar_.Load("Models/bar.obj", "Textures/dark_wood.jpg");
		bar_.transform()->set_scale(Vector3(0.08f, 0.08f, 0.08f));
		bar_.transform()->set_translation(Vector3(0.f, -3.5f, -10.f));

		bottle_.Load("Models/bottle.obj", "Textures/glass.jpg");
		bottle_.transform()->set_scale(Vector3(0.2f, 0.2f, 0.2f));

		cabinet_.Load("Models/cabinet.obj", "Textures/dark_wood.jpg");
		can_.Load("Models/can.obj", "Textures/glass.jpg");

		stool_.Load("Models/stool.obj", "Textures/leather.jpg");
		stool_.transform()->set_scale(Vector3(0.5, 0.5, 0.5));
		stool_.transform()->set_translation(Vector3(-6.f, -4.f, 17.f));

		// Init scene pieces

		// Dance Floor
		dance_floor_.Init(&shape_factory_);

		// Wall Decorations
		wall_decoration_[0].Init(&shape_factory_, light_bar_texture_, Vector3(-6.45f, 0.f, 3.f), 90.f);
		wall_decoration_[1].Init(&shape_factory_, light_bar_texture_, Vector3(-6.45f, 0.f, 0.f), 90.f);
		wall_decoration_[2].Init(&shape_factory_, light_bar_texture_, Vector3(-6.45f, 0.f, -3.f), 90.f);
		wall_decoration_[3].Init(&shape_factory_, light_bar_texture_, Vector3(6.45f, 0.f, 3.f), -90.f);
		wall_decoration_[4].Init(&shape_factory_, light_bar_texture_, Vector3(6.45f, 0.f, 0.f), -90.f);
		wall_decoration_[5].Init(&shape_factory_, light_bar_texture_, Vector3(6.45f, 0.f, -3.f), -90.f);

		// Walls
		shape_factory_.CreateShape(&walls_[0], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture1_);
		walls_[0].transform()->set_scale_x(0.1f);
		walls_[0].transform()->set_translation(Vector3(-8.5f, 1.0, -14.f));
		walls_[0].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[1], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture1_);
		walls_[1].transform()->set_scale_x(0.1f);
		walls_[1].transform()->set_translation(Vector3(-8.5f, 1.0, -4.f));
		walls_[1].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[2], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture1_);
		walls_[2].transform()->set_scale_x(0.1f);
		walls_[2].transform()->set_translation(Vector3(-8.5f, 1.0, 6.f));
		walls_[2].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[3], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture1_);
		walls_[3].transform()->set_scale_x(0.1f);
		walls_[3].transform()->set_translation(Vector3(-8.5f, 1.0, 16.f));
		walls_[3].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[4], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture1_);
		walls_[4].transform()->set_scale_x(0.1f);
		walls_[4].transform()->set_translation(Vector3(8.5f, 1.0, -14.f));
		walls_[4].transform()->set_rotation_y(180.0);

		walls_[4].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[5], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture1_);
		walls_[5].transform()->set_scale_x(0.1f);
		walls_[5].transform()->set_translation(Vector3(8.5f, 1.0, -4.f));
		walls_[5].transform()->set_rotation_y(180.0);
		walls_[5].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[6], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture1_);
		walls_[6].transform()->set_scale_x(0.1f);
		walls_[6].transform()->set_translation(Vector3(8.5f, 1.0, 6.f));
		walls_[6].transform()->set_rotation_y(180.0);
		walls_[6].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[7], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture1_);
		walls_[7].transform()->set_scale_x(0.1f);
		walls_[7].transform()->set_translation(Vector3(8.5f, 1.0, 16.f));
		walls_[7].transform()->set_rotation_y(180.0);
		walls_[7].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[8], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture2_);
		walls_[8].transform()->set_scale_x(1.8f);
		walls_[8].transform()->set_scale_z(0.1f);
		walls_[8].transform()->set_rotation_y(180.0);

		walls_[8].transform()->set_translation(Vector3(0.f, 1.0, 21.5f));
		walls_[8].TileTextureCoords();

		shape_factory_.CreateShape(&walls_[9], ShapeData::CUBE, 10, Vector3(0.f, 0.f, 0.f), NULL, wallpaper_texture2_);
		walls_[9].transform()->set_scale_x(1.8f);
		walls_[9].transform()->set_scale_z(0.1f);
		walls_[9].transform()->set_translation(Vector3(0.f, 1.0, -19.51f));
		walls_[9].TileTextureCoords();

		// Floors
		shape_factory_.CreateShape(&floors_[0], ShapeData::CUBE, 16, Vector3(0.f, 0.f, 0.f), NULL, carpet_texture_);
		floors_[0].transform()->set_scale_y(0.1f);
		floors_[0].transform()->set_scale_z(0.875f);
		floors_[0].transform()->set_translation(Vector3(0.f, -4.8f, 15.f));
		floors_[0].TileTextureCoords();

		shape_factory_.CreateShape(&floors_[1], ShapeData::CUBE, 16, Vector3(0.f, 0.f, 0.f), NULL, carpet_texture_);
		floors_[1].transform()->set_scale_y(0.1f);
		floors_[1].transform()->set_scale_z(0.75f);
		floors_[1].transform()->set_translation(Vector3(0.f, -4.8f, -14.f));
		floors_[1].TileTextureCoords();

		// Ceiling
		Material mat;
		mat.Init(Material::MATT);
		shape_factory_.CreateShape(&ceiling_, ShapeData::CUBE, 20, Vector3(0.f, -130.f, 0.475f), &mat, NULL);
		ceiling_.transform()->set_scale(Vector3(0.9f, 0.05f, 2.1f));
		ceiling_.transform()->set_rotation_z(180.f);

		// Mirrored Shelves Unit
		mirrored_shelves_.Init(&shape_factory_, &bottle_, wood_texture_);

		// Fridge
		fridge_.Init(&shape_factory_, &cabinet_, &can_);
		fridge_.transform()->set_translation(Vector3(-4.42f, -3.7f, -18.8f));

		// Tables
		tables_[0].Init(&shape_factory_, shadow_texture_, NULL, Vector3(0.f, 0.f, 0.f));
		tables_[0].transform()->set_translation(Vector3(0.f, -3.7f, 15.f));

		tables_[1].Init(&shape_factory_, shadow_texture_, NULL, Vector3(0.f, 0.f, 0.f));
		tables_[1].transform()->set_translation(Vector3(5.f, -3.7f, 15.f));

		tables_[2].Init(&shape_factory_, shadow_texture_, NULL, Vector3(0.f, 0.f, 0.f));
		tables_[2].transform()->set_translation(Vector3(-5.f, -3.7f, 15.f));

		// Initialise light
		light_manager_.Init(&shape_factory_);

		// hide mouse
		ShowCursor(false);
		
		// Centre mouse
		input_->set_mouse_pos(screen_centre_);
	}

	void Scene3D::Update(float dt)
	{
		// centre mouse
		input_->set_mouse_pos(screen_centre_);

		// User Input:

		// F1: TOGGLE WIREFRAME MODE ON/OFF
		if (input_->KeyPressed(VK_F1))
		{
			if (wireframe_enabled_)
			{
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				wireframe_enabled_ = false;
			}
			else
			{
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				wireframe_enabled_ = true;
			}
		}

		// F2: LOWER / RAISE DANCEFLOOR
		if (input_->KeyPressed(VK_F2))
		{
			if (dance_floor_.floor_lowered())
			{
				dance_floor_.set_raising_floor(true);
				dance_floor_.set_lowering_floor(false);
			}
			else if (dance_floor_.floor_raised())
			{
				dance_floor_.set_lowering_floor(true);
				dance_floor_.set_raising_floor(false);
			}
		}

		// F3:SWITCH CAMERA
		if (input_->KeyPressed(VK_F3))
		{
			switch (camera_mode_)
			{
			case MAIN:
				camera_mode_ = SECURITY;
				break;
			case SECURITY:
				camera_mode_ = FIXED;
				break;
			case FIXED:
				camera_mode_ = MAIN;
				break;
			}
		}

		// F4:SWITCH LIGHT
		if (input_->KeyPressed(VK_F4))
		{
			light_manager_.SwitchLightMode();
		}

		// F5:TOGGLE FLASHING LIGHT ON/OFF
		if (input_->KeyPressed(VK_F5))
		{
			light_manager_.set_flashing(!light_manager_.flashing());
		}

		// Update Wall Decorations
		for (int i = 0; i < 6; i++)
		{
			wall_decoration_[i].Update(dt);
		}
		
		// update main camera controller
		//camera_controller_main_.Update(dt);
		if (camera_controllers_[camera_mode_] != NULL)
			camera_controllers_[camera_mode_]->Update(dt);

		// update input
		input_->Update();

		// update dancefloor
		dance_floor_.Update(dt);

		// update light manager
		light_manager_.Update(dt);

		// Render scene once all updating has been complete
		Render();
	}

	void Scene3D::Render()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); // Clear The Screen, Depth & Stencil Buffer
		glLoadIdentity(); // load Identity Matrix

		// set glu Lookat in relation to camera
		gluLookAt(
			cameras_[camera_mode_].position().x(), cameras_[camera_mode_].position().y(), cameras_[camera_mode_].position().z(),
			cameras_[camera_mode_].look_at().x(), cameras_[camera_mode_].look_at().y(), cameras_[camera_mode_].look_at().z(),
			cameras_[camera_mode_].up().x(), cameras_[camera_mode_].up().y(), cameras_[camera_mode_].up().z()
			);
		
		light_manager_.Render();

		// Set Matrix Mode to Model View
		// glMatrixMode(GL_MODELVIEW);

		// Draw mirrored shelves
		mirrored_shelves_.Render();

		// Draw lights if wireframe is on
		if (wireframe_enabled_)
		{
			for (int i = 0; i < MAX_LIGHTS; i++)
			{
				light_manager_.RenderLight();
			}
		}
		
		// Draw bar
		bar_.Render(true);

		// Draw fridge
		fridge_.Render();
	
		// Draw dance floor
		dance_floor_.Render();

		// Draw floors
		for (int i = 0; i < MAX_FLOORS; ++i)
		{
			floors_[i].Render();
		}

		// Draw walls
		for (int i = 0; i < MAX_WALLS; ++i)
		{
			walls_[i].Render();
		}

		// Draw ceiling
		ceiling_.Render();
	

		// Draw wall decorations
		for (int i = 0; i < 6; i++)
		{
			wall_decoration_[i].Render();
		}

		// Draw tables
		for (int i = 0; i < MAX_TABLES; ++i)
		{
			tables_[i].Render();
		}
		
		// Draw Stools
		stool_.Render(true);
		glPushMatrix();
			for (int i = 0; i < 2; ++i)
			{
				glTranslatef(5.f, 0.f, 0.f);
				stool_.Render(true);
			}
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0.f, 0.f, -6.f);
			stool_.Render(true);
			glPushMatrix();
				for (int i = 0; i < 2; ++i)
				{
					glTranslatef(5.f, 0.f, 0.f);
					stool_.Render(true);
				}
			glPopMatrix();
		glPopMatrix();

		SwapBuffers(hdc_);// Swap the frame buffers.
	}

	// Resize the window
	void Scene3D::Resize()
	{
		if (hwnd_ == NULL)
			return;

		GetClientRect(*hwnd_, &screen_rect_);
		ResizeGLWindow(screen_rect_.right, screen_rect_.bottom);

		client_centre_ = CalcClientCentre(screen_rect_);
		screen_centre_ = CalcScreenCentre(screen_rect_);
	}

	// Calculate and return centre (screen co-ords)	
	POINT Scene3D::CalcClientCentre(RECT& screen)
	{
		POINT centre;
		centre.x = (long)(screen.right / 2);
		centre.y = (long)(screen.bottom / 2);
		return centre;
	}

	// Calculate and return centre (client co-ords)
	POINT Scene3D::CalcScreenCentre(RECT& screen)
	{
		POINT centre = CalcClientCentre(screen);
		ClientToScreen(*hwnd_, &centre);
		return centre;
	}

}

